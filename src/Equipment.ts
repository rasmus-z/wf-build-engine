import {ModData} from ".";
import {
	ArtifactPolarity,
	DamageType,
	ItemType,
	ModularPartType,
	ProductCategory,
	SlotType,
	UpgradeType,
} from "./enums";
import {
	ArtifactSlot,
	AttackData,
	Behavior,
	BehaviorTypes,
	CompatibilityTag,
	EquipmentData,
	Item,
	ItemDB,
	ItemPath,
	ItemRecipeData,
	KuvaData,
	LocTag,
	ProjectileTypeData,
	RivenData,
	UniqueDamageSource,
	WeaponFireBehavior,
	WeaponImpactBehavior,
	WeaponStateBehavior,
} from "./interfaces";

export const DAMAGE_TYPES = [
	"DT_CORROSIVE",
	"DT_ELECTRICITY",
	"DT_EXPLOSION",
	"DT_FIRE",
	"DT_FREEZE",
	"DT_GAS",
	"DT_IMPACT",
	"DT_MAGNETIC",
	"DT_POISON",
	"DT_PUNCTURE",
	"DT_RADIANT",
	"DT_RADIATION",
	"DT_SLASH",
	"DT_VIRAL",
] as const;

export default class Equipment {
	public static getSlotType(item: Item<EquipmentData>, id: number) {
		const itemType = Equipment.getItemType(item);
		// Moas and Mechs don't have any special slots
		if (Equipment.isMoa(item) || Equipment.isMech(item) || Equipment.isRailjack(item)) {
			return SlotType.NORMAL;
		}
		if (id === 9) {
			if (Equipment.isMelee(item)) {
				return SlotType.STANCE;
			}
			if (Equipment.isPrimary(item) || Equipment.isSecondary(item)) {
				return SlotType.EXILUS;
			}
			if (Equipment.isWarframe(item)) {
				return SlotType.AURA;
			}
			return SlotType.NORMAL;
		}
		if (id === 10 && itemType === ItemType.Warframe) {
			return SlotType.EXILUS;
		}
		if (id === 10 && Equipment.isModular(item)) {
			return SlotType.ARCANE;
		}
		if (id >= 11 && id <= 12) {
			return SlotType.ARCANE;
		}
		return SlotType.NORMAL;
	}

	public static getDefaultStats(
		item: Item<EquipmentData>,
		fireModeIndex: number,
	): {[upgradeType: string]: number} {
		const either = (value: number | undefined, defaultValue: number) =>
			value !== undefined ? value : defaultValue;

		// Defaults should differ based on this.data.item.parent
		const itemType = Equipment.getItemType(item);
		const behaviorIndex =
			item.data.FireModes && item.data.FireModes.length >= fireModeIndex
				? item.data.FireModes[fireModeIndex].behaviorIndex
				: 0;
		switch (itemType) {
			case ItemType.Warframe:
				return {
					AVATAR_HEALTH_MAX: either(item.data.MaxHealthOverride, 100),
					AVATAR_SHIELD_MAX: either(item.data.MaxShieldOverride, 100),
					AVATAR_POWER_MAX: either(item.data.MaxEnergy, 100),
					AVATAR_SPRINT_SPEED: item.data.MovementSpeedMultiplier || 1,
					AVATAR_ARMOUR: either(item.data.ArmourRatingOverride, 50),
					AVATAR_ABILITY_STRENGTH: 1,
					AVATAR_ABILITY_RANGE: 1,
					AVATAR_ABILITY_DURATION: 1,
					AVATAR_ABILITY_EFFICIENCY: 1,
				};
			case ItemType.Weapon:
				return {
					WEAPON_AMMO_MAX: Equipment.getAmmoCapacity(item, behaviorIndex),
					WEAPON_CLIP_MAX: Equipment.getClipSize(item),
					WEAPON_CHARGE_RATE: Equipment.getChargeTime(item, behaviorIndex),
					WEAPON_FIRE_RATE: Equipment.getFireRate(item, behaviorIndex),
					WEAPON_RELOAD_TIME: Equipment.getReloadTime(item, behaviorIndex),
					WEAPON_RELOAD_SPEED: 1,
					WEAPON_CRIT_CHANCE: Equipment.getCriticalChance(item, behaviorIndex),
					WEAPON_DOUBLE_CRIT_CHANCE: 0,
					WEAPON_INIT_DAMAGE_MOD: 1,
					WEAPON_CRIT_DAMAGE: Equipment.getCriticalMultiplier(item, behaviorIndex),
					WEAPON_PUNCTURE_DEPTH: Equipment.getPunctureDepth(item, behaviorIndex),
					WEAPON_FIRE_ITERATIONS: Equipment.getWeaponFireIterations(
						item,
						behaviorIndex,
					),
					WEAPON_SPREAD: Equipment.getWeaponSpread(item, behaviorIndex),
					WEAPON_PROC_CHANCE: Equipment.getWeaponStatusChance(item, behaviorIndex),
					WEAPON_PROC_TIME: 1, // Status Duration
					WEAPON_DAMAGE_IF_VICTIM_PROC_ACTIVE: 1, // Condition Overload
					WEAPON_DAMAGE_AMOUNT: Equipment.getWeaponDamageMultiplier(
						item,
						behaviorIndex,
					),
					WEAPON_MELEE_DAMAGE: 1,
					WEAPON_CHANNELING_DAMAGE: 1,
					WEAPON_CHANNELING_EFFICIENCY: 1,
					WEAPON_PARRY_DAMAGE_BLOCKED: either(item.data.ParryDamagePercentBlocked, 0),
					WEAPON_RANGE: Equipment.getWeaponRange(item, behaviorIndex),
					WEAPON_SLASH_PROC_ON_CRIT_CHANCE: 0,
				};
			case ItemType.Sentinel:
				return {
					AVATAR_ARMOUR: either(item.data.ArmourRatingOverride, 50),
					AVATAR_HEALTH_MAX: either(item.data.MaxHealthOverride, 0),
					AVATAR_SHIELD_MAX: either(item.data.MaxShieldOverride, 0),
				};
			default:
				return {
					AVATAR_ARMOUR: either(item.data.ArmourRatingOverride, 0),
					AVATAR_HEALTH_MAX: either(item.data.MaxHealthOverride, 0),
					AVATAR_SHIELD_MAX: either(item.data.MaxShieldOverride, 0),
				};
		}
	}

	public static getItemType(item: Item<EquipmentData>) {
		if (
			item.data.ProductCategory === ProductCategory.Suits ||
			item.data.ProductCategory === ProductCategory.SpaceSuits ||
			item.data.ProductCategory === ProductCategory.MechSuits
		) {
			return ItemType.Warframe;
		} else if (
			item.data.ProductCategory === ProductCategory.KubrowPets ||
			item.data.ProductCategory === ProductCategory.Sentinels
		) {
			return ItemType.Sentinel;
		}
		return ItemType.Weapon;
	}

	public static getPathBaseName(path: string) {
		const baseIndex = path.lastIndexOf("/");
		if (baseIndex === -1) {
			return path;
		}
		return path.substr(baseIndex + 1);
	}

	public static getBehaviorFromData(
		behaviorData: BehaviorTypes,
		key: "fire",
	): WeaponFireBehavior | null;
	public static getBehaviorFromData(
		behaviorData: BehaviorTypes,
		key: "impact",
	): WeaponImpactBehavior | null;
	public static getBehaviorFromData(
		behaviorData: BehaviorTypes,
		key: "state",
	): WeaponStateBehavior | null;
	public static getBehaviorFromData(
		behaviorData: BehaviorTypes,
		key: "fire" | "impact" | "state",
	): Behavior | null;
	public static getBehaviorFromData(
		behaviorData: BehaviorTypes,
		key: "fire" | "impact" | "state",
	): Behavior | null {
		const type = (behaviorData as {[key: string]: string})[`${key}:Type`];
		if (!type) {
			return null;
		}
		const pathBaseName = Equipment.getPathBaseName(type);
		return (behaviorData as {[key: string]: any})[`${key}:${pathBaseName}`];
	}

	public static getBehavior(
		item: Item<EquipmentData>,
		behaviorNum: number,
		key: "fire",
	): WeaponFireBehavior | null;
	public static getBehavior(
		item: Item<EquipmentData>,
		behaviorNum: number,
		key: "impact",
	): WeaponImpactBehavior | null;
	public static getBehavior(
		item: Item<EquipmentData>,
		behaviorNum: number,
		key: "state",
	): WeaponStateBehavior | null;
	public static getBehavior(
		item: Item<EquipmentData>,
		behaviorNum: number,
		key: "fire" | "impact" | "state",
	): Behavior | null {
		if (!item.data.Behaviors || !item.data.Behaviors[behaviorNum]) {
			return null;
		}
		const behaviorData = item.data.Behaviors[behaviorNum];
		return Equipment.getBehaviorFromData(behaviorData, key);
	}

	public static getWeaponFireBehavior(
		item: Item<EquipmentData>,
		behaviorIndex: number,
	) {
		return Equipment.getBehavior(item, behaviorIndex, "fire");
	}

	public static getWeaponImpactBehavior(
		item: Item<EquipmentData>,
		behaviorIndex: number,
	) {
		return Equipment.getBehavior(item, behaviorIndex, "impact");
	}

	public static getWeaponStateBehavior(
		item: Item<EquipmentData>,
		behaviorIndex: number,
	) {
		return Equipment.getBehavior(item, behaviorIndex, "state");
	}

	public static getClipSize(item: Item<EquipmentData>) {
		return item.data.AmmoClipSize || item.data.PVPAmmoClipSize || 1;
	}

	public static getAmmoCapacity(item: Item<EquipmentData>, behaviorIndex: number) {
		// Fulmin, Shedu, Flux Rifle
		if (Equipment.isRechargeable(item)) {
			return 0;
		}
		// Hema
		if (item.data.UseClipSizeAsMaxAmmo) {
			return 0;
		}
		if (item.data.AmmoCapacity !== undefined) {
			return item.data.AmmoCapacity;
		}
		const fire = Equipment.getWeaponFireBehavior(item, behaviorIndex);
		if (fire === null || fire.ammoType === undefined) {
			return 0;
		}
		// This data comes from Lotus/Types/Player/InventoryController.SupportedAmmoTypes
		switch (fire.ammoType) {
			case "/Lotus/Weapons/Ammo/PistolAmmoEx":
				return 210;
			case "/Lotus/Weapons/Ammo/RifleAmmoEx":
				return 540;
			case "/Lotus/Weapons/Ammo/ShellsAmmoEx":
				return 120;
			case "/Lotus/Weapons/Ammo/SniperAmmoEx":
				return 72;
			case "/Lotus/Weapons/Ammo/HealthAmmoEx":
				return 540;
		}
		return 0;
	}

	// Get a weapon's rate of fire in rounds per second.
	public static getFireRate(item: Item<EquipmentData>, behaviorIndex: number) {
		const state = Equipment.getWeaponStateBehavior(item, behaviorIndex);
		if (state === null) {
			return 0;
		}
		// Sarpa has NumShot and BurstDelay that should only be used in charged attacks and does not affect displayed
		if (state.NumShots && Equipment.isRanged(item)) {
			if (state.BurstDelay) {
				return (
					(1 /
						(1 / ((state.fireRate || 60) / 60) + state.NumShots * state.BurstDelay)) *
					state.NumShots
				);
			} else {
				return ((state.fireRate || 60) * state.NumShots) / 60;
			}
		}
		return (state.fireRate || 60) / 60;
	}

	public static getChargeTime(item: Item<EquipmentData>, behaviorIndex: number) {
		const state = Equipment.getWeaponStateBehavior(item, behaviorIndex);
		if (state && state.ChargeTime) {
			return state.ChargeTime;
		}
		return 0;
	}

	public static canCharge(item: Item<EquipmentData>, behaviorIndex: number) {
		const state = Equipment.getWeaponStateBehavior(item, behaviorIndex);
		if (state && state.ChargeTime) {
			return true;
		}
		return false;
	}

	public static getChargeModifier(item: Item<EquipmentData>, behaviorIndex: number) {
		const state = Equipment.getWeaponStateBehavior(item, behaviorIndex);
		if (state) {
			return state.ChargeModifier;
		}
	}

	public static getBehaviorIndex(item: Item<EquipmentData>, fireModeIndex: number) {
		return item.data.FireModes && item.data.FireModes.length >= fireModeIndex
			? item.data.FireModes[fireModeIndex].behaviorIndex
			: 0;
	}

	// Return whether weapon uses battery instead of ammo
	public static isRechargeable(item: Item<EquipmentData>) {
		if (item.data.ClipIsBattery && item.data.BatteryRegenRate) {
			return true;
		}
		return false;
	}

	// Get seconds after firing stops before battery starts recharging
	public static getRechargeDelay(item: Item<EquipmentData>) {
		return item.data.BatteryRegenDelay || 1;
	}

	public static getRechargeRate(item: Item<EquipmentData>) {
		return item.data.BatteryRegenRate || 1;
	}

	// Get a weapon's reload time in seconds.
	public static getReloadTime(item: Item<EquipmentData>, behaviorIndex: number) {
		if (Equipment.isRechargeable(item) && item.data.AmmoClipSize) {
			// Weapon recharges instead of reloading (Fulmin)
			// Return recharge delay + time to recharge all ammo
			const regenDelay = Equipment.getRechargeDelay(item);
			const regenRate = Equipment.getRechargeRate(item);
			return regenDelay + item.data.AmmoClipSize / regenRate;
		}

		const state = Equipment.getWeaponStateBehavior(item, behaviorIndex);
		if (state === null || state.reloadTime === undefined) {
			return 0;
		}
		return state.reloadTime;
	}

	public static projectileHasData(projectile?: ProjectileTypeData) {
		if (projectile) {
			if (projectile.AttackData && (projectile.AttackData.Amount || 0) > 0) {
				return true;
			}
			if (projectile.ExplosiveAttack && (projectile.ExplosiveAttack.Amount || 0) > 0) {
				return true;
			}
			if (projectile.Embed && !projectile.OwnEmbedEffect) {
				if (projectile.EmbedAttack && (projectile.EmbedAttack.Amount || 0) > 0) {
					return true;
				}
				if (
					projectile.EmbedDeathAttack &&
					(projectile.EmbedDeathAttack.Amount || 0) > 0
				) {
					return true;
				}
			}
		}
		return false;
	}

	public static getProjectileCriticalChance(
		item: Item<EquipmentData>,
		behaviorIndex: number,
	) {
		const fire = Equipment.getWeaponFireBehavior(item, behaviorIndex);
		if (fire === null) {
			return null;
		}

		// Charged projectile sequence list (Deconstructor Prime)
		if (
			fire.chargedProjectileSequenceList &&
			fire.chargedProjectileSequenceList.length > 0 &&
			Equipment.projectileHasData(fire.chargedProjectileSequenceList[0])
		) {
			return fire.chargedProjectileSequenceList[0].CriticalChance || 0;
		}
		// Normal projectile sequence list (Deconstructor)
		if (
			fire.projectileSequenceList &&
			fire.projectileSequenceList.length > 0 &&
			Equipment.projectileHasData(fire.projectileSequenceList[0])
		) {
			return fire.projectileSequenceList[0].CriticalChance || 0;
		}

		if (
			fire.chargedProjectileType &&
			Equipment.projectileHasData(fire.chargedProjectileType)
		) {
			return fire.chargedProjectileType.CriticalChance || 0;
		}
		if (fire.projectileType) {
			return fire.projectileType.CriticalChance || 0;
		}
		return null;
	}

	public static getImpactCriticalChance(
		item: Item<EquipmentData>,
		behaviorIndex: number,
	) {
		const impact = Equipment.getWeaponImpactBehavior(item, behaviorIndex);
		if (impact === null) {
			return null;
		}
		return impact.criticalChance === undefined
			? impact.criticalHitChance === undefined
				? 0
				: impact.criticalHitChance
			: impact.criticalChance;
	}

	public static getCriticalChance(item: Item<EquipmentData>, behaviorIndex: number) {
		const projectileCrit = Equipment.getProjectileCriticalChance(item, behaviorIndex);
		if (projectileCrit !== null) {
			return projectileCrit;
		}
		const impactCrit = Equipment.getImpactCriticalChance(item, behaviorIndex);
		if (impactCrit !== null) {
			return impactCrit;
		}
		return 0;
	}

	public static getProjectileCriticalMultiplier(
		item: Item<EquipmentData>,
		behaviorIndex: number,
	) {
		const fire = Equipment.getWeaponFireBehavior(item, behaviorIndex);
		if (fire === null) {
			return null;
		}
		if (
			fire.chargedProjectileType &&
			Equipment.projectileHasData(fire.chargedProjectileType)
		) {
			return fire.chargedProjectileType.CriticalMultiplier || 0;
		}
		if (fire.projectileType) {
			return fire.projectileType.CriticalMultiplier || 0;
		}
		return null;
	}

	public static getImpactCriticalMultiplier(
		item: Item<EquipmentData>,
		behaviorIndex: number,
	) {
		const impact = Equipment.getWeaponImpactBehavior(item, behaviorIndex);
		if (impact === null) {
			return null;
		}
		return impact.criticalHitDamageMultiplier === undefined
			? 0
			: impact.criticalHitDamageMultiplier;
	}

	public static getCriticalMultiplier(
		item: Item<EquipmentData>,
		behaviorIndex: number,
	) {
		const projectileCrit = Equipment.getProjectileCriticalMultiplier(
			item,
			behaviorIndex,
		);
		if (projectileCrit !== null) {
			return projectileCrit;
		}
		const impactCrit = Equipment.getImpactCriticalMultiplier(item, behaviorIndex);
		if (impactCrit !== null) {
			return impactCrit;
		}
		return 0;
	}

	public static getPunctureDepth(item: Item<EquipmentData>, behaviorIndex: number) {
		const fire = Equipment.getWeaponFireBehavior(item, behaviorIndex);
		if (fire === null) {
			return 0;
		}
		if (
			fire.chargedProjectileType &&
			Equipment.projectileHasData(fire.chargedProjectileType)
		) {
			return fire.chargedProjectileType.BasePunctureDepth || 0;
		}
		if (fire.projectileType) {
			return fire.projectileType.BasePunctureDepth || 0;
		}
		return fire.tracePunctureDepth || 0;
	}

	public static getWeaponFireIterations(
		item: Item<EquipmentData>,
		behaviorIndex: number,
	) {
		const fire = Equipment.getWeaponFireBehavior(item, behaviorIndex);
		if (fire === null || fire.IgnoreFireIterations) {
			return 1;
		}
		return fire.fireIterations || 1;
	}

	private static normalizeAttackData = (
		attackData: AttackData,
		kuvaData?: KuvaData,
	) => {
		const damage: {[damageType: string]: number} = {};
		const useNewFormat = attackData.UseNewFormat;
		DAMAGE_TYPES.forEach((dt) => {
			const dataDamage = attackData[dt] || 0;
			if (useNewFormat || dataDamage > 1) {
				damage[dt] = (damage[dt] || 0) + dataDamage;
			} else {
				damage[dt] =
					(damage[dt] || 0) +
					(dataDamage || (attackData.Type === dt ? 1 : 0)) * (attackData.Amount || 0);
			}
		});
		// Add Kuva damage to attack data
		if (kuvaData) {
			const kuvaDamageType = DamageType[kuvaData[0]];
			const totalDamage = Object.values(damage).reduce(
				(total, amount) => total + amount,
				0,
			);
			const kuvaDamage = totalDamage * kuvaData[1];
			damage[kuvaDamageType] = (damage[kuvaDamageType] || 0) + kuvaDamage;
		}
		return damage;
	};

	public static mergeAttackData = (
		baseAttackData: AttackData,
		attackDataOverrides: AttackData,
	) => {
		// Merge attackDataOverrides onto baseAttackData, overwrite conflicting attributes
		const newAttackData = {...baseAttackData};
		let totalDamage = 0;
		DAMAGE_TYPES.forEach((dt) => {
			const overrideDamage = attackDataOverrides[dt];
			if (overrideDamage !== undefined) {
				newAttackData[dt] = overrideDamage;
			}
			totalDamage += newAttackData[dt] || 0;
		});
		if (attackDataOverrides.Amount) {
			newAttackData.Amount = attackDataOverrides.Amount;
		} else {
			newAttackData.Amount = totalDamage;
		}
		return newAttackData;
	};

	public static hasProjectile(item: Item<EquipmentData>, fireModeIndex: number) {
		const fire = Equipment.getWeaponFireBehavior(item, fireModeIndex);
		if (fire) {
			if (
				(fire.projectileType && Equipment.projectileHasData(fire.projectileType)) ||
				(fire.chargedProjectileType &&
					Equipment.projectileHasData(fire.chargedProjectileType))
			) {
				return true;
			}
		}
		return false;
	}

	public static getWeaponDamage(
		item: Item<EquipmentData>,
		fireModeIndex: number,
		kuvaData?: KuvaData,
	) {
		const damageSources: UniqueDamageSource[] = [];
		const behaviorIndex =
			item.data.FireModes && item.data.FireModes.length >= fireModeIndex
				? item.data.FireModes[fireModeIndex].behaviorIndex
				: 0;
		const fire = Equipment.getWeaponFireBehavior(item, behaviorIndex);

		const damageCategories = [] as Array<{
			name?: string;
			damageSources: UniqueDamageSource[];
		}>;

		const canCharge = Equipment.canCharge(item, fireModeIndex);

		if (fire !== null) {
			const loadProjectileAttackData = ({
				projectile,
				normalProjectile,
				primaryProjectile,
				nameOverride,
				isChargedProjectile,
			}: {
				projectile: ProjectileTypeData;
				normalProjectile?: ProjectileTypeData;
				primaryProjectile?: ProjectileTypeData;
				nameOverride?: string;
				isChargedProjectile?: boolean;
			}) => {
				const projectileDamageSources: UniqueDamageSource[] = [];

				// AttackData
				if (projectile.AttackData) {
					if (primaryProjectile && primaryProjectile.AttackData) {
						// TODO: Only use "QUICK SHOT" if it has a charged  mode
						projectileDamageSources.push({
							name: canCharge
								? isChargedProjectile
									? "/Lotus/Language/Game/ChargedDamage"
									: "/Lotus/Language/Game/QuickShotDamage"
								: "/Lotus/Language/Labels/WEAPON_DAMAGE_AMOUNT",
							attackData: Equipment.normalizeAttackData(
								{
									...primaryProjectile.AttackData,
									...projectile.AttackData,
								},
								kuvaData,
							),
							stats: {
								WEAPON_CRIT_CHANCE:
									primaryProjectile.CriticalChance || projectile.CriticalChance || 0,
								WEAPON_CRIT_DAMAGE:
									primaryProjectile.CriticalMultiplier ||
									projectile.CriticalMultiplier ||
									0,
								WEAPON_PROC_CHANCE: primaryProjectile.AttackData
									? primaryProjectile.AttackData.ProcChance || 0
									: projectile.AttackData
									? projectile.AttackData.ProcChance || 0
									: 0,
							},
							isProjectile: true,
							isChargedProjectile,
						});
					} else if ((projectile.AttackData.Amount || 0) > 0) {
						projectileDamageSources.push({
							name: canCharge
								? isChargedProjectile
									? "/Lotus/Language/Game/ChargedDamage"
									: "/Lotus/Language/Game/QuickShotDamage"
								: "/Lotus/Language/Labels/WEAPON_DAMAGE_AMOUNT",
							attackData: Equipment.normalizeAttackData(
								projectile.AttackData,
								kuvaData,
							),
							stats: {
								WEAPON_CRIT_CHANCE: projectile.CriticalChance || 0,
								WEAPON_CRIT_DAMAGE: projectile.CriticalMultiplier || 0,
								WEAPON_PROC_CHANCE: projectile.AttackData
									? projectile.AttackData.ProcChance || 0
									: 0,
							},
							isProjectile: true,
							isChargedProjectile,
						});
					}
				}
				// ExplosiveAttack (Tonkor)
				if (
					projectile.ExplosiveAttack &&
					(projectile.ExplosiveAttack.Amount || 0) > 0 &&
					projectile.DamageRadius // Buzlok has ExplosiveAttack but doesn't actually explode?
				) {
					// If ExplosiveAttack has no damage type, default to blast (DT_EXPLOSION)
					// EmbedDeathAttack appears to also default to blast damage?
					const explosiveAttack = {...projectile.ExplosiveAttack};
					if (!explosiveAttack.UseNewFormat && !explosiveAttack.Type) {
						/**
						 * TODO: Idenfiy why Tonkor's projectile defaults to Blast damage,
						 * while Staticor's falls back on Radiation
						 * Neither one has a damage type defined for ExplosiveAttack
						 */

						// Staticor appears to inherit charged explosive damage type from uncharged projectile?
						if (
							normalProjectile &&
							normalProjectile.ExplosiveAttack &&
							normalProjectile.ExplosiveAttack.Type
						) {
							explosiveAttack.Type = normalProjectile.ExplosiveAttack.Type;
						} else {
							// Otherwise default to blast damage (see Tonkor blast damage)
							explosiveAttack.Type = "DT_EXPLOSION";
						}
					}
					const missingDamageType = explosiveAttack.UseNewFormat
						? !DAMAGE_TYPES.some((dt) => explosiveAttack[dt])
						: !explosiveAttack.Type;
					if (missingDamageType) {
						explosiveAttack.DT_EXPLOSION = explosiveAttack.UseNewFormat
							? explosiveAttack.Amount
							: 1;
					}
					projectileDamageSources.push({
						name: isChargedProjectile
							? "/Lotus/Language/Game/ChargedRadialAttack"
							: "/Lotus/Language/Labels/AVATAR_RADIAL_ATTACK",
						attackData: Equipment.normalizeAttackData(explosiveAttack, kuvaData),
						stats: {},
					});
				}
				// TODO
				// EmbedDeathAttack (Lenz / Torid)
				//  && !projectile.OwnEmbedEffect // Mutalist Cernos has this set
				if (projectile.Embed) {
					if (projectile.EmbedAttack && (projectile.EmbedAttack.Amount || 0) > 0) {
						const embedAttack = {...projectile.EmbedAttack};
						if (projectile.EmbedTime && projectile.EmbedTime.length > 0) {
							// Add 1 damage tick if DealDamageThroughImpactBehavior is set
							// Stug shows 10 damage: 4 EmbedAttack damage * (1.5 EmbedTime + 1 DealDamageThroughImpactBehavior)
							const embedTime =
								projectile.EmbedTime[0] +
								(projectile.DealDamageThroughImpactBehavior ? 1 : 0);

							// Torid, Stug damage over time
							projectileDamageSources.push({
								name: "DAMAGE OVER TIME",
								attackData: Equipment.normalizeAttackData(
									Equipment.attackDataMultiplyDamage(embedAttack, embedTime),
									kuvaData,
								),
								stats: {},
							});
						} else {
							// An embed attack with no delay is just another radial attack?
							projectileDamageSources.push({
								name: "/Lotus/Language/Labels/AVATAR_RADIAL_ATTACK",
								attackData: Equipment.normalizeAttackData(embedAttack, kuvaData),
								stats: {},
							});
						}
					}
					// EmbedDeathAttack (Lenz)
					if (
						projectile.EmbedTime &&
						projectile.EmbedDeathAttack &&
						(projectile.EmbedDeathAttack.Amount || 0) > 0
					) {
						// Delayed explosion after EmbedTime[0]
						const embedDeathAttack = {...projectile.EmbedDeathAttack};
						if (!embedDeathAttack.UseNewFormat && !embedDeathAttack.Type) {
							embedDeathAttack.Type = "DT_EXPLOSION";
						}

						const missingDamageType = embedDeathAttack.UseNewFormat
							? !DAMAGE_TYPES.some((dt) => embedDeathAttack[dt])
							: !embedDeathAttack.Type;
						if (missingDamageType) {
							embedDeathAttack.DT_EXPLOSION = embedDeathAttack.UseNewFormat
								? embedDeathAttack.Amount
								: 1;
						}
						// Delayed explosion
						// TODO: Show a better label for this
						projectileDamageSources.push({
							name: "/Lotus/Language/Labels/AVATAR_RADIAL_ATTACK", // "/Lotus/Language/Labels/WEAPON_EMBED_DELAY",
							attackData: Equipment.normalizeAttackData(embedDeathAttack, kuvaData),
							stats: {},
						});
					}
				}

				// Bomblings: Projectile splits into a cluster of explosive projectiles
				// Used by Zarr, Kuva Bramma
				if (projectile.ClusterProjectiles && projectile.NumClusterProjectiles) {
					// const bomblingData = loadProjectileAttackData({
					// 	projectile: projectile.ClusterProjectiles,
					// 	nameOverride: "CLUSTER BOMBS",
					// });
					// TODO: call this once, return damage data and add multishot for # bomblings
					// projectileDamageSources.push(...bomblingData);
				}

				return projectileDamageSources;
			};

			const primaryFire = Equipment.getWeaponFireBehavior(item, 0);
			const primaryFireHasProjectile = primaryFire
				? Equipment.projectileHasData(primaryFire.chargedProjectileType) ||
				  Equipment.projectileHasData(primaryFire.projectileType)
				: false;

			const state = Equipment.getWeaponStateBehavior(item, fireModeIndex);
			if (
				fire.chargedProjectileType &&
				Equipment.projectileHasData(fire.chargedProjectileType) &&
				// If minimum charge ratio is 100% then it doesn't have a quick-shot mode
				((state && state.MinChargeRatio === 1) ||
					// Only include Charged projectile if it differs from the normal projectile
					JSON.stringify(fire.chargedProjectileType) !==
						JSON.stringify(fire.projectileType))
			) {
				// Charged Projectile Damage
				damageCategories.push({
					name: "[PH] CHARGED SHOT CAT",
					damageSources: loadProjectileAttackData({
						projectile: fire.chargedProjectileType,
						normalProjectile: fire.projectileType,
						primaryProjectile: primaryFire
							? primaryFire.chargedProjectileType
							: undefined,
						nameOverride: "[PH] CHARGED SHOT",
						isChargedProjectile: true,
					}),
				});
				return damageCategories;
			}

			if (
				fire.projectileType &&
				(Equipment.projectileHasData(fire.projectileType) || primaryFireHasProjectile)
			) {
				// Projectile Damage
				damageCategories.push({
					name: "[PH] QUICK SHOT CAT",
					damageSources: loadProjectileAttackData({
						projectile: fire.projectileType,
						primaryProjectile: primaryFire ? primaryFire.projectileType : undefined,
						nameOverride: "[PH] QUICK SHOT",
					}),
				});
				return damageCategories;
			}

			// Handle projectile sequence lists, which rotate through different attacks
			if (
				fire.chargedProjectileSequenceList &&
				fire.chargedProjectileSequenceList.length > 0
			) {
				// Deconstructor Prime attack sequence
				const projectile = fire.chargedProjectileSequenceList[0];
				if (Equipment.projectileHasData(projectile)) {
					damageCategories.push({
						name: "[PH] CHARGED PROJECTILE SEQUENCE",
						damageSources: loadProjectileAttackData({
							projectile: projectile,
							nameOverride: "[PH] CHARGED PROJECTILE SEQUENCE",
						}),
					});
					return damageCategories;
				}
			}

			if (fire.projectileSequenceList && fire.projectileSequenceList.length > 0) {
				// Deconstructor attack sequence
				// Just return first attack in the list for now
				const projectile = fire.projectileSequenceList[0];
				if (Equipment.projectileHasData(projectile)) {
					damageCategories.push({
						name: "[PH] PROJECTILE SEQUENCE",
						damageSources: loadProjectileAttackData({
							projectile: projectile,
							nameOverride: "[PH] PROJECTILE SEQUENCE",
						}),
					});
					return damageCategories;
				}
			}
		}
		const impact = Equipment.getWeaponImpactBehavior(item, behaviorIndex || 0);
		if (impact !== null) {
			// Melee, beam, ranged impact damage:
			if (impact.AttackData) {
				damageSources.push({
					name: canCharge
						? "/Lotus/Language/Game/ChargedDamage"
						: "/Lotus/Language/Labels/WEAPON_DAMAGE_AMOUNT",
					attackData: Equipment.normalizeAttackData(impact.AttackData, kuvaData),
					stats: {},
				});
			}
			// Opticor radial damage
			if (impact.RadialDamage) {
				const baseAmount = impact.RadialDamage.baseAmount;
				const damagePercent = impact.RadialDamage.DamagePercent;
				const attackData = {} as AttackData;
				let totalDamage = 0;
				DAMAGE_TYPES.forEach((dt) => {
					const damageMultiplier = damagePercent[dt] || 0;
					attackData[dt] = baseAmount * damageMultiplier;
					totalDamage += baseAmount * damageMultiplier;
				});
				attackData.UseNewFormat = 1;
				attackData.Amount = totalDamage;
				damageSources.push({
					name: canCharge
						? "/Lotus/Language/Game/ChargedRadialAttack"
						: "/Lotus/Language/Labels/AVATAR_RADIAL_ATTACK",
					attackData: Equipment.normalizeAttackData(attackData, kuvaData),
					stats: {},
				});
			}
		}

		damageCategories.push({
			name: "[PH] BASE CAT",
			damageSources: damageSources,
		});

		return damageCategories;
	}

	public static getWeaponDamageMultiplier(
		item: Item<EquipmentData>,
		behaviorIndex: number,
	) {
		// Projectile weapons don't have damage multipliers
		if (Equipment.hasProjectile(item, behaviorIndex)) {
			return 1;
		}
		let multiplier = 1;
		// See "Kohm" for a 2x base damage multiplier
		const impact = Equipment.getWeaponImpactBehavior(item, behaviorIndex);
		if (impact && impact.PlayerDamageMultiplier) {
			multiplier *= impact.PlayerDamageMultiplier;
		}
		// FIXME: How is DamageMultiplier used?
		// Corvas has DamageMultiplier: 2, but displays base damage in arsenal
		// DamageMultiplier doesn't apply to projectiles? (Opticor uses it for impact damage)
		const state = Equipment.getWeaponStateBehavior(item, behaviorIndex);
		if (state && state.DamageMultiplier) {
			multiplier *= state.DamageMultiplier;
		}
		return multiplier;
	}

	public static getWeaponStatusChance(
		item: Item<EquipmentData>,
		behaviorIndex: number,
	) {
		const fire = Equipment.getWeaponFireBehavior(item, behaviorIndex);
		if (fire !== null) {
			// If ProcChance is not defined in ExplosiveAttack, charged or projectile
			// then it falls through to the impact behavior? See: Tonkor

			// Charged projectile sequence list (Deconstructor Prime)
			if (
				fire.chargedProjectileSequenceList &&
				fire.chargedProjectileSequenceList.length > 0 &&
				Equipment.projectileHasData(fire.chargedProjectileSequenceList[0]) &&
				fire.chargedProjectileSequenceList[0].AttackData &&
				fire.chargedProjectileSequenceList[0].AttackData.ProcChance !== undefined
			) {
				return fire.chargedProjectileSequenceList[0].AttackData.ProcChance;
			}
			// Normal projectile sequence list (Deconstructor)
			if (
				fire.projectileSequenceList &&
				fire.projectileSequenceList.length > 0 &&
				Equipment.projectileHasData(fire.projectileSequenceList[0]) &&
				fire.projectileSequenceList[0].AttackData &&
				fire.projectileSequenceList[0].AttackData.ProcChance !== undefined
			) {
				return fire.projectileSequenceList[0].AttackData.ProcChance;
			}
			// Charged Projectile status:
			if (
				fire.chargedProjectileType &&
				Equipment.projectileHasData(fire.chargedProjectileType) &&
				fire.chargedProjectileType.AttackData &&
				fire.chargedProjectileType.AttackData.ProcChance !== undefined
			) {
				return fire.chargedProjectileType.AttackData.ProcChance;
			}
			// Projectile status:
			if (fire.projectileType) {
				if (
					fire.projectileType.ExplodeOnImpact &&
					fire.projectileType.ExplosiveAttack &&
					fire.projectileType.ExplosiveAttack.ProcChance
				) {
					return fire.projectileType.ExplosiveAttack.ProcChance;
				}
				if (
					fire.projectileType.AttackData !== undefined &&
					fire.projectileType.AttackData.ProcChance !== undefined
				) {
					return fire.projectileType.AttackData.ProcChance;
				}
			}
		}
		const impact = Equipment.getWeaponImpactBehavior(item, behaviorIndex);
		if (impact !== null) {
			// Melee, beam, ranged impact status:
			if (impact.AttackData) {
				return impact.AttackData.ProcChance || 0;
			}
		}
		return 0;
	}

	public static getWeaponSpread(item: Item<EquipmentData>, behaviorIndex: number) {
		const fire = Equipment.getWeaponFireBehavior(item, behaviorIndex);
		if (
			!fire ||
			!fire.AIMED_ACCURACY ||
			!fire.AIMED_ACCURACY.Spread ||
			!fire.AIMED_ACCURACY.Spread.SHOOTING ||
			!fire.AIMED_ACCURACY.Spread.SHOOTING.range
		) {
			return 1;
		}

		return (
			(fire.AIMED_ACCURACY.Spread.SHOOTING.range[0] +
				fire.AIMED_ACCURACY.Spread.SHOOTING.range[1]) /
			2
		);
	}

	public static getWeaponRange(item: Item<EquipmentData>, behaviorIndex: number) {
		const fire = Equipment.getWeaponFireBehavior(item, behaviorIndex);
		if (fire) {
			// Base melee reach is multiplied by reach mods, then SweepRadius is added to it
			if (fire.DefaultMeleeProxies && fire.DefaultMeleeProxies.MAIN_HAND) {
				const mainHand = fire.DefaultMeleeProxies.MAIN_HAND;
				const baseRange =
					Math.max(0, ...mainHand.LocalPosition) +
					(mainHand.ExtraSweepDistance ? mainHand.ExtraSweepDistance + 0.5 : 0);
				return baseRange;
			}
			// TODO: Ranged weapon traceDistance
			if (fire.traceDistance) {
				return fire.traceDistance;
			}
		}
		return 0;
	}

	public static getOmegaAttenuation(item: Item<EquipmentData>) {
		return item.data.OmegaAttenuation || 0;
	}

	public static hasCompatibilityTag(item: Item<EquipmentData>, tag: CompatibilityTag) {
		return item.data.CompatibilityTags && item.data.CompatibilityTags.includes(tag);
	}

	public static getUpgradeStatTag(stat: keyof typeof UpgradeType): LocTag {
		switch (stat) {
			case "AVATAR_POWER_MAX":
				return "/Lotus/Language/Labels/AVATAR_ABILITY";
			case "AVATAR_SHIELD_MAX":
				return "/Lotus/Language/Labels/AVATAR_SHIELD";
			case "WEAPON_CLIP_MAX":
				return "/Lotus/Language/Labels/WEAPON_CLIP";
			case "WEAPON_CRIT_DAMAGE":
				return "/Lotus/Language/Labels/WEAPON_CRIT_MULTIPLIER";
			case "WEAPON_RELOAD_TIME":
				return "/Lotus/Language/Labels/WEAPON_RELOAD";
			default:
				return `/Lotus/Language/Labels/${stat}`;
		}
	}

	public static getNoiseLevel(item: Item<EquipmentData>, fireModeIndex: number) {
		const behaviorIndex =
			item.data.FireModes && item.data.FireModes.length >= fireModeIndex
				? item.data.FireModes[fireModeIndex].behaviorIndex
				: 0;
		const fire = Equipment.getWeaponFireBehavior(item, behaviorIndex);
		if (fire && fire.IsSilenced) return "/Lotus/Language/Labels/WEAPON_NOISEQUIET";
		return "/Lotus/Language/Labels/WEAPON_NOISELOUD";
	}

	/**
	 * Returns whether an item is a child of, or matches a given package path
	 */
	public static isA = (item: Item<any>, itemType: ItemPath) => {
		if (item.path === itemType) {
			return true;
		}

		// Special case representing any weapon (exact behavior is unverified)
		// Be careful not to create an infinite loop by using this path inside the 'isWeapon' function!
		if (itemType === "/Lotus/Types/Game/LotusWeapon") {
			if (Equipment.isWeapon(item)) {
				return true;
			}
		}

		// "/Lotus/Types/Game/SentinelPowerSuit" mods are compatible with other pets
		// See: Animal Instinct, Pack Leader
		if (
			itemType === "/Lotus/Types/Game/SentinelPowerSuit" &&
			item.parents.includes("/Lotus/Types/Game/Pets/PetPowerSuit")
		) {
			return true;
		}

		return item.parents.includes(itemType);
	};

	public static isMelee = (item: Item<EquipmentData>) => {
		return (
			Equipment.isA(item, "/Lotus/Types/Game/LotusMeleeWeapon") ||
			Equipment.isZaw(item) ||
			Equipment.isA(item, "/Lotus/Types/Game/LotusHybridWeapon") // Dark Split-Sword
		);
	};

	public static isRanged = (item: Item<EquipmentData>) => {
		return (
			Equipment.isA(item, "/Lotus/Weapons/Tenno/LotusBulletWeapon") ||
			Equipment.isSecondary(item) ||
			Equipment.isArchGun(item)
		);
	};

	public static isWeapon = (item: Item<EquipmentData>) => {
		return (
			Equipment.isMelee(item) ||
			Equipment.isRanged(item) ||
			Equipment.isSentinelWeapon(item)
		);
	};

	public static isWarframe = (item: Item<EquipmentData>) => {
		return (
			Equipment.isA(item, "/Lotus/Types/Game/PowerSuit") && !Equipment.isMech(item)
		);
	};

	public static isMech = (item: Item<EquipmentData>) => {
		return Equipment.isA(item, "/Lotus/Powersuits/EntratiMech/BaseMechSuit");
	};

	public static isPrimary = (item: Item<EquipmentData>) => {
		return Equipment.isA(item, "/Lotus/Weapons/Tenno/LotusLongGun");
	};

	public static isSecondary = (item: Item<EquipmentData>) => {
		return (
			Equipment.isA(item, "/Lotus/Weapons/Tenno/Pistol/LotusPistol") ||
			Equipment.isKitgun(item) ||
			// Mesa's Regulators
			Equipment.isA(item, "/Lotus/Types/Weapon/LotusCustomAimWeapon")
		);
	};

	public static isSentinelWeapon = (item: Item<EquipmentData>) => {
		return (
			!!item.data.CompatibilityTags &&
			item.data.CompatibilityTags.includes("SENTINEL_WEAPON")
		);
	};

	public static isArchwing = (item: Item<EquipmentData>) => {
		return Equipment.isA(
			item,
			"/Lotus/Types/Game/FlightJetPackItems/PlayerFlightJetPackItem",
		);
	};

	public static isSentinel = (item: Item<EquipmentData>) => {
		return Equipment.isA(item, "/Lotus/Types/Sentinels/SentinelPowerSuit");
	};

	public static isMoa = (item: Item<EquipmentData>) => {
		return item.data.PartType === ModularPartType.LWPT_MOA_HEAD;
	};

	public static isRobotic = (item: Item<EquipmentData>) => {
		return Equipment.isA(item, "/Lotus/Types/Game/Pets/RoboticPetPowerSuit");
	};

	public static isBeast = (item: Item<EquipmentData>) => {
		return Equipment.isA(item, "/Lotus/Types/Game/Pets/PetPowerSuit");
	};

	public static isKavat = (item: Item<EquipmentData>) => {
		return Equipment.isA(item, "/Lotus/Types/Game/CatbrowPet/CatbrowPetPowerSuit");
	};

	public static isKubrow = (item: Item<EquipmentData>) => {
		return Equipment.isA(item, "/Lotus/Types/Game/KubrowPet/KubrowPetPowerSuit");
	};

	public static isCompanion = (item: Item<EquipmentData>) => {
		return (
			Equipment.isSentinel(item) || Equipment.isBeast(item) || Equipment.isMoa(item)
		);
	};

	public static isArchGun = (item: Item<EquipmentData>) => {
		return Equipment.isA(item, "/Lotus/Weapons/Tenno/Archwing/Primary/ArchGun");
	};

	public static isArchMelee = (item: Item<EquipmentData>) => {
		return Equipment.isA(item, "/Lotus/Weapons/Tenno/Archwing/Melee/ArchMeleeWeapon");
	};

	public static isKitgun = (item: Item<EquipmentData>) => {
		return item.data.PartType === ModularPartType.LWPT_GUN_BARREL;
	};

	public static isZaw = (item: Item<EquipmentData>) => {
		return item.data.PartType === ModularPartType.LWPT_BLADE;
	};

	public static isModular = (item: Item<EquipmentData>) => {
		return Equipment.isKitgun(item) || Equipment.isZaw(item) || Equipment.isMoa(item);
	};

	public static isMod = (item: Item<EquipmentData | ModData>) => {
		return (
			// Mods
			Equipment.isA(item, "/Lotus/Types/Game/LotusArtifactUpgrade") ||
			// Auras
			Equipment.isA(item, "/Lotus/Types/Game/LotusAuraUpgrade") ||
			// Stances
			Equipment.isA(item, "/Lotus/Types/LotusMeleeTree/LotusMeleeTree") ||
			// Pet Precepts
			Equipment.isA(item, "/Lotus/Types/Game/SentinelPrecept") ||
			// Moa Precepts
			Equipment.isA(item, "/Lotus/Types/Game/SentinelPrecept/MoaPetPrecept")
		);
	};

	public static isExalted = (item: Item<EquipmentData>) => {
		return (
			item.data.IsAbilityWeapon ||
			(!!item.data.CompatibilityTags &&
				item.data.CompatibilityTags.includes("POWER_WEAPON"))
		);
	};

	public static isBlueprint = (item: Item<EquipmentData>) => {
		return Equipment.isA(item, "/Lotus/Types/Game/RecipeItem");
	};

	public static isRelic = (item: Item<EquipmentData>) => {
		return Equipment.isA(item, "/Lotus/Types/Game/VoidProjectionItem");
	};

	public static isResource = (item: Item<EquipmentData>) => {
		return Equipment.isA(item, "/Lotus/Types/Items/MiscItems/ResourceItem");
	};

	public static isForma = (item: Item<EquipmentData>) => {
		return Equipment.isA(item, "/Lotus/Types/Items/MiscItems/Forma");
	};

	public static isRailjack = (item: Item<EquipmentData>) => {
		return Equipment.isA(item, "/Lotus/Types/Game/CrewShip/Ships/RailJack");
	};

	public static isModdable = (item: Item<EquipmentData>) => {
		return (
			!!item.data.ArtifactSlots ||
			Equipment.isWeapon(item) ||
			Equipment.isWarframe(item) ||
			Equipment.isCompanion(item) ||
			Equipment.isArchwing(item) ||
			Equipment.isModular(item)
		);
	};

	public static isKuva = (item: Item<EquipmentData>) => {
		return !!item.data.IsKuva;
	};

	public static getBlueprintItem = (
		item: Item<EquipmentData>,
		itemDB: ItemDB<EquipmentData>,
	) => {
		return item.data.resultItemType && item.data.resultItemType in itemDB
			? itemDB[item.data.resultItemType]
			: null;
	};

	public static getBlueprint = (
		item: Item<EquipmentData>,
		itemDB: ItemDB<EquipmentData>,
	) => {
		if (item && item.storeData && item.storeData.DisplayRecipe) {
			const blueprint = itemDB[item.storeData.DisplayRecipe] as Item<ItemRecipeData>;
			if (blueprint && blueprint.data.Ingredients) {
				return blueprint;
			}
		}
	};

	public static getBlueprintData = (
		item: Item<EquipmentData>,
		itemDB: ItemDB<EquipmentData>,
	) => {
		const blueprint = Equipment.getBlueprint(item, itemDB);
		if (blueprint) {
			return blueprint.data;
		}
	};

	public static getIngredients = (
		item: Item<EquipmentData>,
		itemDB: ItemDB<EquipmentData>,
	) => {
		const blueprint = Equipment.getBlueprint(item, itemDB);
		if (blueprint && blueprint.data.Ingredients) {
			return blueprint.data.Ingredients.map((ingredient) => {
				return {
					ItemCount: ingredient.ItemCount,
					ItemType: ingredient.ItemType,
					ItemData: itemDB[ingredient.ItemType],
				};
			});
		}
		return [];
	};

	private static usesNewFormat(attackData: AttackData) {
		if (attackData.UseNewFormat) {
			return true;
		}
		if (attackData.UseNewFormat === 0) {
			return false;
		}
		return !!DAMAGE_TYPES.find((dt) => {
			const value = attackData[dt] || 0;
			return value > 0.99;
		});
	}

	public static attackDataAddDamage = (
		data: AttackData,
		addDamage: number,
	): AttackData => {
		const numDamageTypes = DAMAGE_TYPES.reduce((numDt, dt) => {
			const value = data[dt];
			if (value && value > 0) {
				numDt += 1;
			}
			return numDt;
		}, 0);
		const newData = {...data, Amount: (data.Amount || 0) + addDamage} as AttackData;
		if (Equipment.usesNewFormat(data)) {
			const splitDamage = addDamage / numDamageTypes;
			DAMAGE_TYPES.forEach((dt) => {
				const value = data[dt];
				if (value && value > 0) {
					newData[dt] = Math.max(0, value + splitDamage);
				}
			});
		}
		return newData;
	};

	public static attackDataMultiplyDamage = (
		data: AttackData,
		multiplier: number,
	): AttackData => {
		const newData = {...data, Amount: (data.Amount || 0) * multiplier};
		const totalDamage = Equipment.usesNewFormat(data) ? 1 : data.Amount || 0;
		DAMAGE_TYPES.forEach((dt) => {
			const value = data[dt];
			if (value && value > 0) {
				newData[dt] = value * multiplier * totalDamage;
			}
		});
		return newData;
	};

	public static attackDataSetDamage = (
		data: AttackData,
		newDamage: number,
	): AttackData => {
		// Scale zaw damage by taking the original relative values and multiplying by new amount
		const newData = {...data, Amount: newDamage} as AttackData;
		const totalDamage = Equipment.usesNewFormat(data) ? data.Amount || 1 : 1;
		DAMAGE_TYPES.forEach((dt) => {
			const value = data[dt];
			if (value && value > 0) {
				newData[dt] = (value / totalDamage) * newDamage;
			}
		});
		return newData;
	};

	public static getMaxRank(item: Item<EquipmentData>) {
		return Math.max(item.data.LevelCap ? item.data.LevelCap : 30, 30);
	}

	public static getDefaultArtifactSlots(item: Item<EquipmentData>): ArtifactSlot[] {
		const defaultSlots = item.data.ArtifactSlots;
		// Return default polarities
		if (defaultSlots) {
			return defaultSlots;
		}
		// Companions have 10 slots
		if (Equipment.isCompanion(item)) {
			if (Equipment.isMoa(item)) {
				return [
					"AP_PRECEPT",
					"AP_UNIVERSAL",
					"AP_UNIVERSAL",
					"AP_UNIVERSAL",
					"AP_PRECEPT",
					"AP_PRECEPT",
					"AP_UNIVERSAL",
					"AP_UNIVERSAL",
					"AP_UNIVERSAL",
					"AP_PRECEPT",
				];
			} else {
				return Array(10).fill("AP_UNIVERSAL");
			}
		}
		// No slots defined, fall back to 8 unpolarized slots
		const slots = Array(8).fill("AP_UNIVERSAL");
		// Default polarity for Primary and Secondary Exilus slots is AP_ATTACK
		if (Equipment.isPrimary(item) || Equipment.isSecondary(item)) {
			slots.push("AP_ATTACK");
		}
		return slots;
	}

	public static getDefaultPolarities(item: Item<EquipmentData>) {
		return Equipment.getDefaultArtifactSlots(item).map(
			(polarity) => ArtifactPolarity[polarity],
		);
	}

	public static getDisposition(item: Item<EquipmentData>) {
		return item.data.OmegaAttenuation || 1;
	}

	public static getRivenManifest(
		item: Item<EquipmentData>,
		RivenDB: ItemDB<RivenData>,
	) {
		for (const path of Object.keys(RivenDB)) {
			const rivenManifest = RivenDB[path];
			const itemCompatibilities = rivenManifest.data.SentinelItemCompatibilities
				? [
						...rivenManifest.data.ItemCompatibilities,
						...rivenManifest.data.SentinelItemCompatibilities,
				  ]
				: rivenManifest.data.ItemCompatibilities;
			const compatibleRiven = itemCompatibilities.find(
				(compatibleItem) =>
					compatibleItem.Rarity !== "NONE" &&
					Equipment.isA(item, compatibleItem.ItemType), //item.path === compatibleItem.ItemType,
			);
			if (compatibleRiven) {
				return rivenManifest;
			}
		}
	}

	public static getBaseRivenType(
		item: Item<EquipmentData>,
		RivenDB: ItemDB<RivenData>,
	) {
		for (const path of Object.keys(RivenDB)) {
			const rivenManifest = RivenDB[path];
			const itemCompatibilities = rivenManifest.data.SentinelItemCompatibilities
				? [
						...rivenManifest.data.ItemCompatibilities,
						...rivenManifest.data.SentinelItemCompatibilities,
				  ]
				: rivenManifest.data.ItemCompatibilities;
			const compatibleRiven = itemCompatibilities.find(
				(compatibleItem) =>
					compatibleItem.Rarity !== "NONE" &&
					Equipment.isA(item, compatibleItem.ItemType), //item.path === compatibleItem.ItemType,
			);
			if (compatibleRiven) {
				return compatibleRiven.ItemType;
			}
		}
	}

	/**
	 * Returns an exalted weapon if warframe has one
	 * FIXME: This is possibly not the correct way to to this,
	 * AdditionalItems is a list of items the player receives when building the item
	 * SpecialSlotItem is a list of items equipped when an ability is activated
	 */
	public static getExaltedWeapons = (
		item: Item<EquipmentData>,
		itemDB: ItemDB<EquipmentData>,
	) => {
		const exaltedWeapons = [];
		if (item.data.AdditionalItems) {
			for (const itemPath of item.data.AdditionalItems) {
				const additionalItem = itemDB[itemPath];
				if (additionalItem && Equipment.isExalted(additionalItem)) {
					exaltedWeapons.push(additionalItem);
				}
			}
		}
		return exaltedWeapons;
	};

	/**
	 * Returns associated Warframe for an Exalted Weapon
	 */
	public static getExaltedWeaponWarframe = (
		queryItem: Item<EquipmentData>,
		itemDB: ItemDB<EquipmentData>,
	) => {
		if (Equipment.isExalted(queryItem)) {
			for (const warframe of Object.values(itemDB)) {
				if (
					warframe.tag === "Warframe" &&
					Equipment.getExaltedWeapons(warframe, itemDB).find(
						(exaltedWeapon) => exaltedWeapon.path === queryItem.path,
					)
				) {
					return warframe;
				}
			}
		}
		return null;
	};
}
