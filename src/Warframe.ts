import {EquipmentData, Item} from "./interfaces";

interface IItemDB<T> {
	[key: string]: Item<T>;
}

export default class Warframe {
	public static getMaxRank(item: Item<EquipmentData>) {
		return item.data.LevelUpgrades.length - 1;
	}
}
