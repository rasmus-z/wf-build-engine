import {
	ArtifactPolarity,
	InventorySlot,
	KitgunGunType,
	ModularPartType,
	PHYSICAL_DAMAGE_TYPE,
	PHYSICAL_DAMAGE_TYPES,
} from "./enums";
import Equipment from "./Equipment";
import {
	AttackData,
	BehaviorTypes,
	EquipmentData,
	Item,
	KitgunBarrelData,
	KitgunClipData,
	KitgunHandleData,
	LocTag,
	MoaHeadData,
	UpgradeData,
	WeaponFireBehavior,
	ZawGripType,
	ZawHandleData,
	ZawLinkData,
	ZawStrikeData,
} from "./interfaces";

type slotId = 0 | 1 | 2;

interface IZawData extends EquipmentData {
	ArtifactSlots: Array<keyof typeof ArtifactPolarity>;
	Behaviors: IZawBehavior[];
	GripType?: ZawGripType;
	MeleeTreeType?: string;
	MeleeStyle?: string;
}

interface IZawBehavior extends BehaviorTypes {
	"impact:MeleeImpactBehavior": {
		AttackData: AttackData;
		criticalHitChance?: number;
		criticalHitDamageMultiplier?: number;
	};
	"impact:Type": string;
	"fire:Type": string;
	"fire:WeaponMeleeSweepFireBehavior": WeaponFireBehavior;
	"state:WeaponStateBehavior": {
		fireRate: number;
	};
	"state:Type": string;
}

export default class ModularPart {
	public static getPartTypeForSlot(
		item: Item<EquipmentData>,
		slotId: slotId,
	): ModularPartType[] | undefined {
		if (Equipment.isKitgun(item)) {
			switch (slotId) {
				case 0:
					return [ModularPartType.LWPT_GUN_BARREL];
				case 1:
					return [
						ModularPartType.LWPT_GUN_SECONDARY_HANDLE,
						ModularPartType.LWPT_GUN_PRIMARY_HANDLE,
					];
				case 2:
					return [ModularPartType.LWPT_GUN_CLIP];
			}
		}
		if (Equipment.isZaw(item)) {
			switch (slotId) {
				case 0:
					return [ModularPartType.LWPT_BLADE];
				case 1:
					// Zaw grips have no PartType defined, how should this be handled?
					return undefined; // ModularPartType.LWPT_HILT
				case 2:
					return [ModularPartType.LWPT_HILT_WEIGHT];
			}
		}
		return [ModularPartType.LWPT_INVALID];
	}

	/**
	 * Returns LocTag for modular part slot
	 * @param slotId For a kitgun: 0 for CHAMBER, 1 for GRIP or 2 for LOADER
	 */
	public static getPartLabel(item: Item<EquipmentData>, slotId: slotId): LocTag {
		if (Equipment.isKitgun(item)) {
			switch (slotId) {
				case 0:
					return "/Lotus/Language/SolarisVenus/Gun_Barrels";
				case 1:
					return "/Lotus/Language/SolarisVenus/Gun_Handles";
				case 2:
					return "/Lotus/Language/SolarisVenus/Gun_Clips";
			}
		}
		if (Equipment.isZaw(item)) {
			switch (slotId) {
				case 0:
					return "/Lotus/Language/OstronCrafting/Crafting_PartBlade";
				case 1:
					return "/Lotus/Language/OstronCrafting/Crafting_PartHilt";
				case 2:
					return "/Lotus/Language/OstronCrafting/Crafting_PartBalance";
			}
		}
		return "/Lotus/Language/OstronCrafting/Crafting_PartsTitle";
	}

	// Combine barrel, handle, clip into one Item
	public static buildKitgun(
		barrel: Item<KitgunBarrelData>,
		handle?: Item<KitgunHandleData>,
		clip?: Item<KitgunClipData>,
	): Item<EquipmentData> {
		const newData = JSON.parse(JSON.stringify(barrel.data)) as KitgunBarrelData;
		const isPrimary = !!handle && handle.data.PartType === "LWPT_GUN_PRIMARY_HANDLE";
		const behavior =
			isPrimary && newData.PrimaryBehaviorOverrides
				? newData.PrimaryBehaviorOverrides
				: newData.BehaviorOverrides;

		if (behavior) {
			const fire = Equipment.getBehaviorFromData(behavior, "fire");
			const impact = Equipment.getBehaviorFromData(behavior, "impact");
			const state = Equipment.getBehaviorFromData(behavior, "state");

			if (isPrimary) {
				if (newData.PrimaryBehaviorOverrides && fire) {
					/**
					 * This seems like an insane way to do this and is possibly a bug,
					 * but it's the only way to make these numbers match the game.
					 * 1) Start with the stats from BehaviorOverrides
					 * 2) Merge PrimaryBehaviorOverrides onto it, replacing existing attributes
					 * 3) PrimaryBehaviorOverrides' ChargedProjectileType and ProjectileType both
					 *    inherit ProjectileType's stats from BehaviorOverrides, so merge those separately
					 */
					const baseBehavior = newData.BehaviorOverrides;
					const baseFire = baseBehavior
						? Equipment.getBehaviorFromData(baseBehavior, "fire")
						: null;
					if (baseFire && baseFire.projectileType && fire) {
						if (baseFire.projectileType.AttackData && fire.chargedProjectileType) {
							if (fire.chargedProjectileType.AttackData) {
								fire.chargedProjectileType.AttackData = Equipment.mergeAttackData(
									baseFire.projectileType.AttackData,
									fire.chargedProjectileType.AttackData,
								);
							}
							if (
								baseFire.projectileType.ExplosiveAttack &&
								fire.chargedProjectileType.ExplosiveAttack
							) {
								fire.chargedProjectileType.ExplosiveAttack = Equipment.mergeAttackData(
									baseFire.projectileType.ExplosiveAttack,
									fire.chargedProjectileType.ExplosiveAttack,
								);
							}
						}
					}
				}
			}

			// GunType can be undefined (Rattleguts), which apparently defaults to GT_RIFLE
			const barrelGunType = barrel.data.GunType || KitgunGunType.GT_RIFLE;
			if (handle) {
				const handleData = handle.data.Overrides.find((override) => {
					return (
						override.BarrelType === barrel.data.BarrelType &&
						override.GunType === barrelGunType
					);
				});
				if (handleData) {
					if (fire) {
						fire.traceDistance =
							(fire.traceDistance || 0) + (handleData.TraceDistance || 0);
					}
					if (state) {
						state.fireRate = handleData.FireRate;
					}
					// Handle hitscan/beam weapons
					if (impact && impact.AttackData && handleData.Damage !== 0) {
						impact.AttackData = Equipment.attackDataAddDamage(
							impact.AttackData,
							handleData.Damage,
						);
					}
					// Handle projectile weapons (Catchmoon)
					if (fire && fire.projectileType && fire.projectileType.AttackData) {
						fire.projectileType.AttackData = Equipment.attackDataAddDamage(
							fire.projectileType.AttackData,
							handleData.Damage,
						);

						// Explosive Attack (Tombfinger)
						if (fire.projectileType.ExplosiveAttack) {
							fire.projectileType.ExplosiveAttack = Equipment.attackDataAddDamage(
								fire.projectileType.ExplosiveAttack,
								handleData.Damage,
							);
						}
					}

					// Charged projectile weapons (Tombfinger)
					if (
						fire &&
						fire.chargedProjectileType &&
						fire.chargedProjectileType.AttackData
					) {
						fire.chargedProjectileType.AttackData = Equipment.attackDataAddDamage(
							fire.chargedProjectileType.AttackData,
							handleData.Damage,
						);

						// Explosive Attack (Tombfinger)
						if (fire.chargedProjectileType.ExplosiveAttack) {
							fire.chargedProjectileType.ExplosiveAttack = Equipment.attackDataAddDamage(
								fire.chargedProjectileType.ExplosiveAttack,
								handleData.Damage,
							);
						}
					}

					// FIXME: Primary Tombfinger charge time manual overrides
					// These grips add WEAPON_CHARGE_RATE through Upgrades and should be fixed to use those
					if (
						isPrimary &&
						state &&
						barrel.path ==
							"/Lotus/Weapons/SolarisUnited/Secondary/SUModularSecondarySet1/Barrel/SUModularSecondaryBarrelBPart"
					) {
						// Brash
						if (
							handle.path ==
							"/Lotus/Weapons/SolarisUnited/Primary/SUModularPrimarySet1/Handles/SUModularPrimaryHandleAPart"
						) {
							state.ChargeTime = 0.5;
						}
						// Shrewd
						if (
							handle.path ==
							"/Lotus/Weapons/SolarisUnited/Primary/SUModularPrimarySet1/Handles/SUModularPrimaryHandleBPart"
						) {
							state.ChargeTime = 0.8;
						}
						// Steadyslam
						if (
							handle.path ==
							"/Lotus/Weapons/SolarisUnited/Primary/SUModularPrimarySet1/Handles/SUModularPrimaryHandleCPart"
						) {
							state.ChargeTime = 1.1;
						}
						// Tremor
						if (
							handle.path ==
							"/Lotus/Weapons/SolarisUnited/Primary/SUModularPrimarySet1/Handles/SUModularPrimaryHandleDPart"
						) {
							state.ChargeTime = 1.4;
						}
					}

					if (handle.data.Upgrades) {
						newData.ModularUpgrades = [...handle.data.Upgrades];
					}
				}
			}
			if (clip) {
				const clipData = clip.data.Overrides.find(
					(override) =>
						override.BarrelType === barrel.data.BarrelType &&
						override.GunType === barrelGunType,
				);
				if (clipData) {
					newData.AmmoClipSize = clipData.MagazineSize;

					if (state) {
						state.reloadTime = clipData.ReloadTime;
					}

					// Non-projectile weapons
					if (impact && impact.AttackData) {
						impact.AttackData.ProcChance = clipData.ProcChance;
						impact.criticalHitChance = clipData.CritChance;
						impact.criticalHitDamageMultiplier = clipData.CritMultiplier;
					}
					// Projectile weapons
					if (fire && fire.projectileType && fire.projectileType.AttackData) {
						fire.projectileType.AttackData.ProcChance = clipData.ProcChance;
						fire.projectileType.CriticalChance = clipData.CritChance;
						fire.projectileType.CriticalMultiplier = clipData.CritMultiplier;
					}
					// Charged projectile weapons (tombfinger)
					if (
						fire &&
						fire.chargedProjectileType &&
						fire.chargedProjectileType.AttackData
					) {
						fire.chargedProjectileType.AttackData.ProcChance = clipData.ProcChance;
						fire.chargedProjectileType.CriticalChance = clipData.CritChance;
						fire.chargedProjectileType.CriticalMultiplier = clipData.CritMultiplier;
					}
				}
			}
			newData.Behaviors = [behavior];
		}

		newData.CompatibilityTags = ["MODULAR_GUN", "ASSAULT_AMMO"].concat(
			...(newData.CompatibilityTags || []),
		);

		if (isPrimary && newData.PrimaryOmegaAttenuation) {
			newData.OmegaAttenuation = newData.PrimaryOmegaAttenuation;
		}

		newData.ArtifactSlots = [
			"AP_UNIVERSAL",
			"AP_UNIVERSAL",
			"AP_UNIVERSAL",
			"AP_UNIVERSAL",
			"AP_UNIVERSAL",
			"AP_UNIVERSAL",
			"AP_UNIVERSAL",
			"AP_UNIVERSAL",
			"AP_UNIVERSAL", // exilus
			"AP_UNIVERSAL", // arcane
		];

		const parents = isPrimary
			? [
					barrel.parent,
					// Treat primary Catchmoon as a shotgun
					barrel.data.GunType === "GT_SHOTGUN"
						? "/Lotus/Weapons/Tenno/Shotgun/LotusShotgun"
						: "/Lotus/Weapons/Tenno/Rifle/LotusRifle",
					"/Lotus/Weapons/Tenno/LotusLongGun",
			  ]
			: [
					barrel.parent,
					"/Lotus/Weapons/SolarisUnited/Secondary/LotusModularSecondaryShotgun",
					"/Lotus/Weapons/Tenno/Pistol/LotusSinglePistolShotgun",
					"/Lotus/Weapons/Tenno/Pistol/LotusSinglePistolGun",
					"/Lotus/Weapons/Tenno/Pistol/LotusSinglePistol",
					"/Lotus/Weapons/Tenno/Pistol/LotusPistol",
			  ];
		return {
			...barrel,
			parents: parents,
			data: newData,
		} as Item<EquipmentData>;
	}

	public static applyUpgradeOperation = (
		attribute: number | undefined,
		operationType: string,
		value: number,
	) => {
		if (operationType === "ADD") {
			return (attribute || 0) + value;
		}
		if (operationType === "MULTIPLY") {
			return (attribute || 0) * value;
		}
		return value;
	};

	public static buildZaw(
		strike: Item<ZawStrikeData>,
		handle?: Item<ZawHandleData>,
		link?: Item<ZawLinkData>,
	): Item<EquipmentData> {
		const attackData = {...strike.data.AttackData};
		/**
		 * AttackData can infer missing IPS damage (as of patch 27)
		 * If total damage % adds up to less than 100%,
		 * and one of the IPS damage types is missing,
		 * then assume missing damage type makes up the remainder
		 * See Balla for a strike without Puncture damage
		 */
		const baseDamage = attackData.Amount || 0;
		if (!attackData.UseNewFormat) {
			const missingDamage = PHYSICAL_DAMAGE_TYPES.reduce(
				(damage: {percent: number; type?: PHYSICAL_DAMAGE_TYPE}, damageType) => {
					if (damageType in attackData) {
						damage.percent += attackData[damageType]!;
					} else {
						damage.type = damageType;
					}
					return damage;
				},
				{
					percent: 0,
				},
			);
			if (missingDamage.type && missingDamage.percent < 0.99) {
				attackData[missingDamage.type] = 1 - missingDamage.percent;
			}
		}
		const weaponData = {
			ArtifactSlots: [
				"AP_UNIVERSAL",
				"AP_UNIVERSAL",
				"AP_UNIVERSAL",
				"AP_UNIVERSAL",
				"AP_UNIVERSAL",
				"AP_UNIVERSAL",
				"AP_UNIVERSAL",
				"AP_UNIVERSAL",
				"AP_UNIVERSAL", // stance
				"AP_UNIVERSAL", // arcane
			],
			Behaviors: [
				{
					"fire:Type": "/EE/Types/Game/WeaponMeleeSweepFireBehavior",
					"fire:WeaponMeleeSweepFireBehavior": {
						DefaultMeleeProxies: {
							MAIN_HAND: {
								// TODO: Confirm default one-handed zaw range
								LocalPosition: [0, 1, 0],
							},
						},
						fireIterations: 1,
						SweepRadius: strike.data.SweepRadius || 0.25,
					},
					"impact:MeleeImpactBehavior": {
						AttackData: attackData,
						criticalHitChance: strike.data.BaseCritChance || 0,
						criticalHitDamageMultiplier: strike.data.BaseCritMultiplier || 2,
					},
					"impact:Type": "/Lotus/Types/Game/MeleeImpactBehavior",
					"state:Type": "/EE/Types/Game/WeaponStateBehavior",
					"state:WeaponStateBehavior": {
						fireRate: 55,
					},
				},
			],
			IconTexture: strike.data.IconTexture,
			InventorySlot: InventorySlot.SLOT_6,
			LevelUpgrades: [], // todo: Fix this being a required attribute on EquipmentData
			LocalizeDescTag: strike.data.LocalizeDescTag,
			LocalizeTag: strike.data.LocalizeTag,
			ModularUpgrades: [],
			ParryDamagePercentBlocked: 0.8,
			PartType: strike.data.PartType,
			ProductCategory: "Melee",
		} as IZawData;
		const behavior = weaponData.Behaviors[0];
		const upgrades: UpgradeData[] = [];
		let scaledDamage = baseDamage;
		let damageMultiplier = 1;
		if (strike.data.Upgrades) {
			upgrades.push(...strike.data.Upgrades);
		}
		if (handle) {
			if (handle.data.Upgrades) {
				upgrades.push(...handle.data.Upgrades);
			}
			behavior["state:WeaponStateBehavior"].fireRate = handle.data.BaseFireRate || 60;
			weaponData.GripType = handle.data.GripType || ZawGripType.MELEE_ONE_HAND;
			if (handle.data.GripType === ZawGripType.MELEE_TWO_HAND) {
				if (strike.data.TwoHanded) {
					weaponData.CompatibilityTags =
						strike.data.TwoHanded.meleeTree.CompatibilityTags;
					weaponData.ArtifactSlots[8] = strike.data.TwoHanded.stancePolarity;
					if (strike.data.TwoHanded.damageAttenuation) {
						damageMultiplier += strike.data.TwoHanded.damageAttenuation - 1;
					}
					if (strike.data.TwoHanded.SweepInfo) {
						behavior["fire:WeaponMeleeSweepFireBehavior"].DefaultMeleeProxies =
							strike.data.TwoHanded.SweepInfo;
					}
					weaponData.MeleeStyle = strike.data.TwoHanded.meleeStyle;
				}
			} else if (strike.data.OneHanded) {
				weaponData.CompatibilityTags =
					strike.data.OneHanded.meleeTree.CompatibilityTags;
				weaponData.ArtifactSlots[8] = strike.data.OneHanded.stancePolarity;
				if (strike.data.OneHanded.damageAttenuation) {
					damageMultiplier += strike.data.OneHanded.damageAttenuation - 1;
				}
				if (strike.data.OneHanded.SweepInfo) {
					behavior["fire:WeaponMeleeSweepFireBehavior"].DefaultMeleeProxies =
						strike.data.OneHanded.SweepInfo;
				}
				weaponData.MeleeStyle = strike.data.OneHanded.meleeStyle;
			}
		}
		if (link) {
			if (link.data.Upgrades) {
				upgrades.push(...link.data.Upgrades);
			}
		}

		const impactBehavior = behavior["impact:MeleeImpactBehavior"];
		for (const upgrade of upgrades) {
			switch (upgrade.UpgradeType) {
				case "WEAPON_MELEE_DAMAGE":
					if (upgrade.OperationType === "ADD") {
						scaledDamage += upgrade.Value;
					} else if (upgrade.OperationType === "MULTIPLY") {
						damageMultiplier += upgrade.Value;
					}
					break;
				case "WEAPON_PROC_CHANCE":
					attackData.ProcChance = this.applyUpgradeOperation(
						attackData.ProcChance,
						upgrade.OperationType,
						upgrade.Value,
					);
					break;
				case "WEAPON_CRIT_CHANCE":
					impactBehavior.criticalHitChance = this.applyUpgradeOperation(
						impactBehavior.criticalHitChance,
						upgrade.OperationType,
						upgrade.Value,
					);
					break;
				case "WEAPON_CRIT_DAMAGE":
					impactBehavior.criticalHitDamageMultiplier = this.applyUpgradeOperation(
						impactBehavior.criticalHitDamageMultiplier,
						upgrade.OperationType,
						upgrade.Value,
					);
					break;
				case "WEAPON_FIRE_RATE":
					behavior["state:WeaponStateBehavior"].fireRate = this.applyUpgradeOperation(
						behavior["state:WeaponStateBehavior"].fireRate,
						upgrade.OperationType,
						upgrade.Value,
					);
					break;
				default:
					weaponData.ModularUpgrades!.push(upgrade);
			}
		}

		// Apply accumulated damage multipliers from upgrades and damageAttenuation
		scaledDamage *= damageMultiplier;
		const damageDiff = Math.round(scaledDamage - baseDamage);
		behavior["impact:MeleeImpactBehavior"].AttackData = Equipment.attackDataAddDamage(
			attackData,
			damageDiff,
		);

		return {
			data: weaponData,
			id: strike.id,
			parent: strike.parent,
			parents: [
				strike.parent,
				"/Lotus/Weapons/Tenno/Melee/PlayerMeleeWeapon",
				"/Lotus/Types/Game/LotusMeleeWeapon",
			],
			path: strike.path,
			tag: "WeaponParts",
		} as Item<EquipmentData>;
	}

	// Base Zaw stats? /Lotus/Weapons/Ostron/Melee/LotusModularWeapon
	// /Lotus/Weapons/SolarisUnited/Secondary/LotusModularSecondary
	// /Lotus/Weapons/SolarisUnited/Secondary/LotusModularSecondaryBeam
	// /Lotus/Weapons/SolarisUnited/Secondary/LotusModularSecondaryShotgun

	public static buildMoa(
		head: Item<MoaHeadData>,
		// todo: The rest of the Moa
	): Item<EquipmentData> {
		const newData = JSON.parse(JSON.stringify(head.data)) as MoaHeadData;

		newData.ArtifactSlots = [
			"AP_PRECEPT",
			"AP_UNIVERSAL",
			"AP_UNIVERSAL",
			"AP_UNIVERSAL",
			"AP_PRECEPT",
			"AP_PRECEPT",
			"AP_UNIVERSAL",
			"AP_UNIVERSAL",
			"AP_UNIVERSAL",
			"AP_PRECEPT",
		];

		newData.CompatibilityTags = ["MOA_MOD"].concat(
			...(newData.CompatibilityTags || []),
		);

		const parents = [
			head.parent,
			"/Lotus/Types/Friendly/Pets/MoaPets/MoaPetPowerSuit",
			"/Lotus/Types/Game/Pets/RoboticPetPowerSuit",
			"/Lotus/Types/Game/SentinelPowerSuit",
		];
		return {
			...head,
			parents: parents,
			data: newData,
		} as Item<EquipmentData>;
	}
}
