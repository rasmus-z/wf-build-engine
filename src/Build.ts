import {
	ArtifactPolarity,
	DamageType,
	ELEMENTAL_DAMAGE_TYPES,
	OperationType,
	SlotPolarity,
	SlotType,
	UpgradeType,
} from "./enums";
import Equipment from "./Equipment";
import {
	EquipmentData,
	Item,
	ItemPath,
	ModData,
	RivenData,
	UniqueDamageSource,
	UpgradeData,
} from "./interfaces";
import Mod from "./Mod";
import Riven from "./Riven";

export interface RivenSlotData {
	polarity?: ArtifactPolarity;
	masteryRank: number;
	rerolls: number;
	buffs?: Array<[number, number]>;
	curse?: [number, number] | null;
}

export interface BuildData {
	applyConditionals?: boolean;
	item: Item<EquipmentData>;
	fireModeIndex: number;
	rank: number;
	orokin: boolean;
	slots: Array<{
		polarity?: ArtifactPolarity;
		mod?: Item<ModData>;
		rank?: number;
		rivenData?: RivenSlotData;
	}>;
	kuvaData?: [DamageType, number];
}

type DamageTypeString = keyof typeof DamageType;

export interface BuildSlot {
	id: number;
	drain: number;
	polarity: ArtifactPolarity;
	polarityMatch: SlotPolarity;
	mod?: Item<ModData>;
	elementalDamageTypes: DamageTypeString[];
	rank: number;
	rivenData?: RivenSlotData;
	type: SlotType;
	hasForma?: boolean;
	defaultPolarity?: ArtifactPolarity;
}

const PRIMARY_DAMAGE_TYPE_MAP: {[key in DamageTypeString]?: DamageTypeString[]} = {
	DT_CORROSIVE: ["DT_ELECTRICITY", "DT_POISON"],
	DT_EXPLOSION: ["DT_FIRE", "DT_FREEZE"],
	DT_GAS: ["DT_FIRE", "DT_POISON"],
	DT_MAGNETIC: ["DT_FREEZE", "DT_ELECTRICITY"],
	DT_RADIATION: ["DT_FIRE", "DT_ELECTRICITY"],
	DT_VIRAL: ["DT_FREEZE", "DT_POISON"],
};

const SECONDARY_DAMAGE_TYPE_MAP: {
	[key in DamageTypeString]?: {[key in DamageTypeString]?: DamageTypeString}
} = {
	DT_ELECTRICITY: {
		DT_FIRE: "DT_RADIATION",
		DT_FREEZE: "DT_MAGNETIC",
		DT_POISON: "DT_CORROSIVE",
	},
	DT_FIRE: {
		DT_ELECTRICITY: "DT_RADIATION",
		DT_FREEZE: "DT_EXPLOSION",
		DT_POISON: "DT_GAS",
	},
	DT_FREEZE: {
		DT_ELECTRICITY: "DT_MAGNETIC",
		DT_FIRE: "DT_EXPLOSION",
		DT_POISON: "DT_VIRAL",
	},
	DT_POISON: {
		DT_ELECTRICITY: "DT_CORROSIVE",
		DT_FIRE: "DT_GAS",
		DT_FREEZE: "DT_VIRAL",
	},
};

const STATUS_DURATION_MAP: {[damageType: string]: number | undefined} = {
	DT_CORROSIVE: 8,
	DT_ELECTRICITY: 6,
	DT_EXPLOSION: 6,
	DT_FIRE: 6,
	DT_FREEZE: 6,
	DT_GAS: 6,
	DT_IMPACT: 6,
	DT_MAGNETIC: 6,
	DT_POISON: 6,
	DT_PUNCTURE: 6,
	DT_RADIATION: 12,
	DT_SLASH: 6,
	DT_VIRAL: 6,
};

// A Build is a collection of upgrades applied to an item
// such as a Warframe or a Weapon.
export default class Build {
	public capacity: number = 0;
	public maxCapacity: number = 0;
	public formas: number = 0;
	public endo: number = 0;
	public stats: {[upgradeType: string]: number} = {};
	public damage: {[damageType: string]: number} = {};
	public splitDamage: UniqueDamageSource[] = [];
	public damageUpgrades: {[damageType: string]: number} = {};
	public weightedStatusChance: {[damageType: string]: number} = {};
	public slots: BuildSlot[] = [];
	public totalDamage: number = 0;
	public addedMeleeWeaponDamage: number = 0; // See Covert Lethality mod behavior
	public derivedStats = {
		averageDamage: 0,
		baseMultishot: 0,
		burstDamage: 0,
		damageReduction: 0,
		effectiveHitPoints: 0,
		statusChanceAfterMultishot: 0,
		statusChanceBeforeMultishot: 0,
		sustainedDamage: 0,
	};
	public modSetNumEquipped: {
		[ModSet: string]: number;
	} = {};
	public conditionals: boolean = false;

	constructor(public data: BuildData) {
		this.calculateStats();
	}

	public getCapacityMultiplier() {
		return this.data.orokin ? 2 : 1;
	}

	public getCapacity() {
		const usage = 0;
		const maximum = this.getCapacityMultiplier() * this.data.rank;
		return this.slots.reduce(
			(capacity, slot) => {
				if (!slot.mod) {
					return capacity;
				}
				const drain = Mod.getDrain(
					slot.mod,
					slot.rank || 0,
					slot.polarity,
					slot.rivenData,
				);
				if (drain < 0) {
					// Aura mods increase maximum capacity:
					capacity.maximum -= drain;
				} else {
					// All other mods increase capacity usage:
					capacity.usage += drain;
				}
				return capacity;
			},
			{usage, maximum},
		);
	}

	public calculateFormas() {
		const defaultPolarities = Equipment.getDefaultPolarities(this.data.item);
		// List of polarities that aren't in their default slots
		const defaultNormalPolarities = defaultPolarities.filter(
			(polarity, slotIndex) =>
				this.slots[slotIndex] &&
				this.slots[slotIndex].type === SlotType.NORMAL &&
				polarity !== this.slots[slotIndex].polarity,
		);

		// You can't move an existing polarity without applying at least 1 forma
		const minimumForma = defaultNormalPolarities.length > 0 ? 1 : 0;

		// Count number of required formas
		const formas = this.slots.reduce((formaCount, slot, slotIndex) => {
			slot.defaultPolarity = defaultPolarities[slotIndex];
			if (slot.polarity !== defaultPolarities[slotIndex]) {
				if (
					slot.type === SlotType.AURA ||
					slot.type === SlotType.STANCE ||
					slot.type === SlotType.EXILUS
				) {
					// Stance, Aura and Exilus slot can't move its polarity
					// If polarity doesn't match default, needs forma
					slot.hasForma = true;
					return formaCount + 1;
				} else if (slot.type === SlotType.NORMAL) {
					const defaultIndex = defaultNormalPolarities.findIndex(
						(portablePolarity) => portablePolarity === slot.polarity,
					);
					if (defaultIndex > -1) {
						// Found a matching default polarity, doesn't need a forma
						defaultNormalPolarities.splice(defaultIndex, 1);
						return formaCount;
					}
					// No matching default polarity, needs forma
					slot.hasForma = true;
					return formaCount + 1;
				}
			}
			return formaCount;
		}, 0);
		this.formas = Math.max(minimumForma, formas);
		if (
			(Equipment.isKitgun(this.data.item) || Equipment.isMoa(this.data.item)) &&
			this.formas >= 1
		) {
			// Kitguns and Moas start with 1 freely-polarized slot, so subtract 1 forma
			this.formas -= 1;
		}

		// Infested companions from Heart of Deimos receive a freely polarized slot from gilding
		const modularInfestedCompanion =
			Equipment.isA(
				this.data.item,
				// Vulpaphyla Kavat
				"/Lotus/Types/Friendly/Pets/CreaturePets/BaseInfestedCatbrowPetPowerSuit",
			) ||
			Equipment.isA(
				this.data.item,
				// Predasite Kubrow
				"/Lotus/Types/Friendly/Pets/CreaturePets/BasePredatorKubrowPetPowerSuit",
			);
		if (modularInfestedCompanion && this.formas >= 1) {
			this.formas -= 1;

			// Subtract 1 forma to simulate using Mutagen/Antigen to change initial slot polarity
			if (
				this.formas >= 1 &&
				this.slots.find((slot) => {
					return (
						slot.polarity === ArtifactPolarity.AP_DEFENSE ||
						slot.polarity === ArtifactPolarity.AP_ATTACK ||
						slot.polarity === ArtifactPolarity.AP_TACTIC ||
						slot.polarity === ArtifactPolarity.AP_PRECEPT
					);
				})
			) {
				this.formas -= 1;
			}
		}
	}

	/**
	 * Returns list of modStates using fewest forma to come under max build capacity
	 */
	public getOptimizedFormas = () => {
		const build = this;
		const modState = this.data.slots;
		const defaultPolarities = Equipment.getDefaultPolarities(build.data.item);
		const defaultSlotStates = modState.map((oldState, slotIndex) => {
			const polarity = defaultPolarities[slotIndex];
			return {
				mod: {
					...oldState,
					modId: oldState.mod ? oldState.mod.id : undefined,
					polarity,
				},
				slotId: slotIndex + 1,
			};
		});

		interface ISlotState {
			slot: BuildSlot;
			modState: {
				slotId: number;
				mod?: {
					modId?: number;
					polarity?: ArtifactPolarity;
					rank?: number;
					rivenData?: RivenSlotData;
				};
			};
		}

		const sortSlotsByDrain = (a: ISlotState, b: ISlotState) => {
			// Sort slots to put mods with highest drain first
			const modA = a.slot.mod;
			const modB = b.slot.mod;
			if (modA && modB) {
				const drainA = Mod.getDrain(
					modA,
					Mod.getMaxRank(modA),
					// TODO: Determine whether it's better to sort by mod's forma'd drain or base drain
					ArtifactPolarity.AP_UNIVERSAL, // Mod.getPolarity(modA, a.slot.rivenData)
				);
				const drainB = Mod.getDrain(
					modB,
					Mod.getMaxRank(modB),
					ArtifactPolarity.AP_UNIVERSAL,
				);
				return drainB - drainA;
			}
			if (modA) {
				return -1;
			}
			if (modB) {
				return 1;
			}
			return 0;
		};

		const mods = defaultSlotStates
			.map((slotState) => {
				const slot = build.slots[slotState.slotId - 1];
				if (slot && slot.type === SlotType.NORMAL) {
					// Reset slot polarities to neutral if slot type is normal
					return {
						mod: {
							...slotState.mod,
							polarity: ArtifactPolarity.AP_UNIVERSAL,
						},
						slotId: slotState.slotId,
					};
				}
				return slotState;
			})
			.map((slotState) => {
				return {
					slot: build.slots[slotState.slotId - 1],
					modState: slotState,
				};
			})
			.sort(sortSlotsByDrain);

		const formadSlots: number[] = [];
		const polarizeBestSlot = ({
			targetPolarity,
			targetSlotType,
		}: {
			targetPolarity?: ArtifactPolarity;
			targetSlotType?: SlotType;
		}) => {
			// TODO: Prioritize type of forma to use, eg. only use rarer Umbra forma as a last resort, prefer V and -- polarities?
			if (typeof targetPolarity !== "undefined") {
				// 1) Find first unpolarized slot with matching mod polarity
				let appliedForma = false;
				for (let i = 0; i < mods.length; i++) {
					const mod = mods[i];
					// Slot is not already forma'd
					if (mod.modState.mod.polarity === ArtifactPolarity.AP_UNIVERSAL) {
						if (
							(typeof targetSlotType === "undefined" &&
								mod.slot.type === SlotType.NORMAL) ||
							mod.slot.type === targetSlotType
						) {
							if (mod.slot.mod) {
								// If we have a mod and its polarity matches, apply forma
								const modPolarity = Mod.getPolarity(mod.slot.mod, mod.slot.rivenData);
								const polarityMatch = Mod.getPolarityMatch(targetPolarity, modPolarity);
								if (polarityMatch === SlotPolarity.MATCH) {
									// Set slot polarity to provided polarity
									mod.modState.mod.polarity = targetPolarity;
									appliedForma = true;
									break;
								}
							} else {
								// We reached a blank slot without finding a match, apply forma
								// Try to put the polarity in its original slot if possible
								for (let j = i; j < mods.length; j++) {
									const emptyMod = mods[j];
									if (
										defaultPolarities[emptyMod.modState.slotId - 1] ===
											targetPolarity &&
										emptyMod.modState.mod.polarity === ArtifactPolarity.AP_UNIVERSAL
									) {
										// Found matching default slot for this polarity, prioritize it
										emptyMod.modState.mod.polarity = targetPolarity;
										appliedForma = true;
										break;
									}
								}
								if (!appliedForma) {
									mod.modState.mod.polarity = targetPolarity;
									appliedForma = true;
								}
								break;
							}
						}
					}
				}
				// 2) We didn't find a match or a blank slot to forma, apply it to the mod with the lowest drain
				// Iterate backwards over slots until we find a slot without a polarity
				if (!appliedForma) {
					for (let i = mods.length - 1; i >= 0; i--) {
						const mod = mods[i];
						// Slot is not already forma'd
						if (
							mod.slot.type === targetSlotType &&
							mod.modState.mod.polarity === ArtifactPolarity.AP_UNIVERSAL
						) {
							mod.modState.mod.polarity = targetPolarity;
							appliedForma = true;
							break;
						}
					}
				}
			} else {
				// We aren't distributing default polarities, forma the first thing we come across
				let formadSlot = 0;
				for (let i = 0; i < mods.length; i++) {
					const mod = mods[i];
					// Slot is not already forma'd
					if (!formadSlots.includes(mod.modState.slotId)) {
						if (
							typeof targetSlotType === "undefined" ||
							mod.slot.type === targetSlotType
						) {
							if (mod.slot.mod) {
								const modPolarity = Mod.getPolarity(mod.slot.mod, mod.slot.rivenData);
								mod.modState.mod.polarity = modPolarity;
								formadSlot = mod.modState.slotId;
								break;
							}
						}
					}
				}
				if (formadSlot) {
					formadSlots.push(formadSlot);
				}
			}
			mods.sort(sortSlotsByDrain);
		};

		const getCapacity = () => {
			const maxCapacity = build.getCapacityMultiplier() * build.data.rank;
			const capacity = mods.reduce(
				(capacity, mod) => {
					const slot = mod.slot;
					if (!slot.mod) {
						return capacity;
					}
					const drain = Mod.getDrain(
						slot.mod,
						slot.rank || 0,
						mod.modState.mod.polarity,
						slot.rivenData,
					);
					if (drain < 0) {
						// Aura mods increase maximum capacity:
						capacity.maximum -= drain;
					} else {
						// All other mods increase capacity usage:
						capacity.usage += drain;
					}
					return capacity;
				},
				{usage: 0, maximum: maxCapacity},
			);
			return capacity.maximum - capacity.usage;
		};

		// First, move item's default polarities to slots with best matching mods
		defaultSlotStates
			.filter((defaultSlotState) => {
				const buildSlot = build.slots[defaultSlotState.slotId - 1];
				if (
					buildSlot &&
					buildSlot.type === SlotType.NORMAL &&
					defaultSlotState.mod.polarity !== ArtifactPolarity.AP_UNIVERSAL
				) {
					return true;
				}
				return false;
			})
			.forEach((slotState) => {
				console.log("providedPolarity", slotState);
				polarizeBestSlot({
					targetPolarity: slotState.mod.polarity,
					targetSlotType: SlotType.NORMAL,
				});
			});

		// Call polarizeBestSlot() until we're below max capacity
		// Prioritize polarizing Aura, Stance, Normal, then Exilus slots
		if (getCapacity() < 0) {
			polarizeBestSlot({targetSlotType: SlotType.AURA});
		}
		if (getCapacity() < 0) {
			polarizeBestSlot({targetSlotType: SlotType.STANCE});
		}
		for (let i = 0; i < mods.length; i++) {
			if (getCapacity() >= 0) {
				break;
			}
			polarizeBestSlot({targetSlotType: SlotType.NORMAL});
		}
		if (getCapacity() < 0) {
			polarizeBestSlot({targetSlotType: SlotType.EXILUS});
		}

		// Return optimized slot polarities
		return mods.map((mod) => {
			return mod.modState;
		});
	};

	public isMatchingValidType(itemType: ItemPath) {
		if (!itemType || Equipment.isA(this.data.item, itemType)) {
			return true;
		}
		return false;
	}

	public getFactionDamage() {
		// Return highest amount of faction-specific damage in the build
		const disposition = this.data.item.data.OmegaAttenuation || 1;
		return this.slots.reduce((factionDamage, slot) => {
			if (slot.mod) {
				for (const upgrade of Mod.getPassiveUpgrades(
					slot.mod,
					this.data.applyConditionals,
				)) {
					if (
						upgrade.UpgradeType === "GAMEPLAY_FACTION_DAMAGE" &&
						(upgrade.SymbolFilter === "Grineer" ||
							upgrade.SymbolFilter === "Corpus" ||
							upgrade.SymbolFilter === "Infestation")
					) {
						const value = (1 + (slot.rank || 0)) * upgrade.Value;
						if (value > factionDamage) {
							return value;
						}
					}
				}
				if (slot.rivenData) {
					for (const upgrade of Riven.getPassiveUpgrades(
						slot.mod as Item<RivenData>,
						slot.rivenData,
						disposition,
					)) {
						if (upgrade.UpgradeType === "GAMEPLAY_FACTION_DAMAGE") {
							const value = (1 + (slot.rank || 0)) * upgrade.Value;
							if (value > factionDamage) {
								if (upgrade.SymbolFilter === "Grineer") {
									// Bias Grineer damage so it will roll over other factions
									return value + 0.01;
								}
								return value;
							}
						}
					}
				}
			}
			return factionDamage;
		}, 0);
	}

	public *getModularUpgrades(): IterableIterator<[UpgradeData, number]> {
		if (this.data.item.data.ModularUpgrades) {
			for (const upgrade of this.data.item.data.ModularUpgrades) {
				yield [upgrade, 1];
			}
		}
	}

	public *getPassiveUpgrades(): IterableIterator<[UpgradeData, number]> {
		if (this.data.item.data.LevelUpgrades) {
			const upgrades = this.data.item.data.LevelUpgrades;
			const rank = this.data.rank;
			const maxUpgradeRank = upgrades.length - 1;
			const clampedRank = rank < maxUpgradeRank ? rank : maxUpgradeRank;
			for (let i = 0; i <= clampedRank; i++) {
				yield [upgrades[i], 1];
			}
		}
		for (const slot of this.getSlotsInUIOrder()) {
			const mod = slot.mod;
			if (!mod) {
				continue;
			}

			/**
			 * Handle Coaction Drift aura upgrades
			 * If mod is an aura, find any mods that buff aura effects
			 */
			let auraMultiplier = 1;
			if (slot.type === SlotType.AURA) {
				for (const otherSlot of this.getSlotsInUIOrder()) {
					if (otherSlot.mod && otherSlot.type !== SlotType.AURA) {
						const modSetNumEquipped =
							otherSlot.mod.data.ModSet &&
							this.modSetNumEquipped[otherSlot.mod.data.ModSet];
						for (const upgrade of Mod.getPassiveUpgrades(
							otherSlot.mod,
							this.data.applyConditionals,
						)) {
							if (
								upgrade.UpgradeType === "AVATAR_AURA_STRENGTH" ||
								upgrade.UpgradeType === "AVATAR_AURA_EFFECTIVENESS_ON_ME"
							) {
								const upgradeValue =
									upgrade.Value *
									(1 + (otherSlot.rank || 0)) *
									Mod.getModSetMultiplier(otherSlot.mod, modSetNumEquipped || 0);
								if (upgrade.OperationType === "STACKING_MULTIPLY") {
									auraMultiplier *= 1 + upgradeValue;
								}
							}
						}
					}
				}
			}

			const numEquippedModsInSet = mod.data.ModSet
				? this.modSetNumEquipped[mod.data.ModSet]
				: 0;
			for (const upgrade of Mod.getPassiveUpgrades(mod, this.data.applyConditionals)) {
				yield [
					upgrade,
					(1 + (slot.rank || 0)) *
						Mod.getModSetMultiplier(mod, numEquippedModsInSet) *
						// If aura, apply Coaction Drift multiplier
						(slot.type === SlotType.AURA ? auraMultiplier : 1),
				];
			}
			if (
				mod.data.ModSet &&
				mod.data.ModSetData &&
				// Exalted weapons do not benefit from set bonuses
				!Equipment.isExalted(this.data.item)
			) {
				// ModSet bonus upgrades should only be counted once
				// Look through other slots to see if we've already counted this mod's set bonus
				for (const otherSlot of this.getSlotsInUIOrder()) {
					if (otherSlot.mod && otherSlot.mod.data.ModSet === mod.data.ModSet) {
						if (otherSlot.mod.id === mod.id) {
							// we reached our mod without running into another mod in the set..
							for (const upgrade of Mod.getPassiveUpgrades(
								{...mod, data: mod.data.ModSetData},
								this.data.applyConditionals,
							)) {
								yield [upgrade, numEquippedModsInSet];
							}
						} else {
							break;
						}
					}
				}
			}

			if (slot.rivenData) {
				const disposition = Equipment.getDisposition(this.data.item);
				for (const rivenUpgrade of Riven.getPassiveUpgrades(
					slot.mod as Item<RivenData>,
					slot.rivenData,
					disposition,
				)) {
					yield [rivenUpgrade, 1 + (slot.rank || 0)];
				}
			}

			if (this.data.applyConditionals) {
				// Hack to apply Berserker's 75% attack speed increase
				if (mod.path === "/Lotus/Upgrades/Mods/Melee/WeaponCritFireRateBonusMod") {
					yield [
						{
							OperationType: "MULTIPLY",
							UpgradeType: "WEAPON_FIRE_RATE",
							Value: 1.75,
						} as UpgradeData,
						1,
					];
				}
				// Hack to apply Growing Power's 25% ability strength
				else if (
					mod.path ===
					"/Lotus/Upgrades/Mods/Aura/FairyQuest/FairyQuestCritToAbilityAuraMod"
				) {
					yield [
						{
							OperationType: "STACKING_MULTIPLY",
							UpgradeType: "AVATAR_ABILITY_STRENGTH",
							Value: 0.25,
						} as UpgradeData,
						1,
					];
				}
			}
		}
	}

	public getModSetNumEquipped(mod: Item<ModData>) {
		return this.slots.reduce((numSetMods, slot) => {
			if (slot.mod && slot.mod.data.ModSet === mod.data.ModSet) {
				numSetMods += 1;
			}
			return numSetMods;
		}, 0);
	}

	private calculateModSetNumEquipped() {
		this.modSetNumEquipped = {};
		for (const slot of this.slots) {
			const modSet = slot.mod && slot.mod.data && slot.mod.data.ModSet;
			if (slot.mod && modSet) {
				this.modSetNumEquipped[modSet] = this.getModSetNumEquipped(slot.mod);
			}
		}
	}

	private calculateStats() {
		this.calculateSlots();
		this.calculateCapacity();
		this.calculateEndo();
		this.calculateFormas();
		this.calculateModSetNumEquipped();
		this.calculateUpgradeValues();
		this.calculateDamage();
	}

	private calculateSlots() {
		const item = this.data.item;
		const defaultPolarities = Equipment.getDefaultPolarities(item);
		const slots = this.data.slots;

		defaultPolarities.map((polarity, slotIndex) => {
			if (!slots[slotIndex]) {
				slots[slotIndex] = {
					polarity: polarity,
					rank: 0,
				};
			}
		});

		this.slots = slots.map((slot, slotIndex) => {
			const slotPolarity =
				slot.polarity !== undefined ? slot.polarity : defaultPolarities[slotIndex];

			return {
				drain: ((s) =>
					s.mod ? Mod.getDrain(s.mod, s.rank || 0, slotPolarity, slot.rivenData) : 0)(
					slot,
				),
				elementalDamageTypes: slot.mod
					? Mod.getElementalDamageTypes(
							slot.mod,
							this.data.applyConditionals,
							slot.rivenData,
					  )
					: [],
				id: slotIndex + 1,
				mod: slot.mod,
				polarity: slotPolarity,
				polarityMatch: slot.mod
					? Mod.getPolarityMatch(
							slotPolarity,
							Mod.getPolarity(slot.mod, slot.rivenData),
					  )
					: SlotPolarity.NEUTRAL,
				rank: slot.rank || 0,
				rivenData: slot.rivenData,
				type: Equipment.getSlotType(this.data.item, slotIndex + 1),
			};
		});

		this.conditionals = true;
	}

	private calculateCapacity() {
		const capacity = this.getCapacity();
		this.maxCapacity = capacity.maximum;
		this.capacity = capacity.maximum - capacity.usage;
	}

	private calculateEndo() {
		this.endo = this.slots.reduce((endo, slot) => {
			if (slot.mod && !Mod.isArcane(slot.mod)) {
				const rarity = Mod.getRarity(slot.mod);
				const rank = slot.rank || 0;
				if (rank > 0) {
					endo += 10 * (rarity + 1) * (2 ** rank - 1);
				}
			}
			return endo;
		}, 0);
	}

	private evaluateUpgradeOperations(
		setValue: number,
		addedValue: number,
		multiplier: number,
		stackingMultiplier: number,
	) {
		return (
			(setValue || 0) * (multiplier || 1) * (1 + (stackingMultiplier || 0)) +
			(addedValue || 0)
		);
	}

	private applyUpgradesToStats(
		setValues: {[upgradeType: string]: number},
		upgrades: IterableIterator<[UpgradeData, number]>,
	) {
		const upgradeTypes: {[index: string]: UpgradeType} = {};
		(Object.keys(setValues) as Array<keyof typeof UpgradeType>).forEach((typeName) => {
			upgradeTypes[typeName] = UpgradeType[typeName];
		});
		const addedValues: {[key: string]: number} = {};
		const multipliers: {[key: string]: number} = {};
		const stackingMultipliers: {[key: string]: number} = {};
		this.addedMeleeWeaponDamage = 0;
		for (const [upgradeData, multiplier] of upgrades) {
			if (
				upgradeData.UpgradeType === "NONE" ||
				!this.isMatchingValidType(upgradeData.ValidType)
			) {
				continue;
			}
			let op = OperationType[upgradeData.OperationType];
			const damageType = DamageType[upgradeData.DamageType];
			const damageTypeI = damageType === DamageType.DT_ANY ? 0 : 1 + damageType;
			const typeName =
				upgradeData.UpgradeType +
				(damageTypeI !== 0 ? `,${upgradeData.DamageType}` : "");
			// Sometimes the data doesn't bother setting ADD when that's the only
			// sensible choice.
			if (upgradeData.UpgradeType === "WEAPON_PERCENT_BASE_DAMAGE_ADDED") {
				op = OperationType.ADD;
			}
			const type = UpgradeType[upgradeData.UpgradeType] | (damageTypeI << 16);
			upgradeTypes[typeName] = type;
			const value = upgradeData.Value * multiplier;
			if (op === OperationType.ADD) {
				if (typeName === "WEAPON_MELEE_DAMAGE") {
					// ADD'd melee damage needs to be applied directly to weapon's base damage
					// See Covert Lethality mod behavior
					this.addedMeleeWeaponDamage += value;
				} else {
					addedValues[type] = (addedValues[type] || 0) + value;
				}
			} else if (op === OperationType.MULTIPLY) {
				multipliers[type] = (multipliers[type] || 1) * value;
			} else if (op === OperationType.STACKING_MULTIPLY) {
				stackingMultipliers[type] = (stackingMultipliers[type] || 0) + value;
			} else if (op === OperationType.SET) {
				setValues[UpgradeType[type]] = value;
			}
		}

		for (const typeName in upgradeTypes) {
			const type = upgradeTypes[typeName];
			this.stats[typeName] = this.evaluateUpgradeOperations(
				setValues[typeName],
				addedValues[type],
				multipliers[type],
				stackingMultipliers[type],
			);
		}

		const item = this.data.item;

		// Override multishot upgrades if IgnoreFireIterations is set
		const behaviorIndex = this.data.fireModeIndex;
		const fireMode = Equipment.getWeaponFireBehavior(item, behaviorIndex);
		let baseMultishot = setValues.WEAPON_FIRE_ITERATIONS || 1;
		if (fireMode && fireMode.IgnoreFireIterations) {
			this.stats.WEAPON_FIRE_ITERATIONS = 1;
			baseMultishot = 1;
		}

		/**
		 * Phage and Ocucor have 1 extra central beam unaffected by multishot
		 * Add it in after upgrades have been applied
		 */
		if (fireMode && fireMode.CentralBeam && fireMode.UseInnerRing) {
			this.stats.WEAPON_FIRE_ITERATIONS += 1;
			baseMultishot += 1;
		}
		this.derivedStats.baseMultishot = baseMultishot;

		// Bows cannot have their magazine size increased by mods
		if (item.data.HasClip === 0) {
			this.stats.WEAPON_CLIP_MAX = Equipment.getClipSize(item);
		}

		// Prevent magazine size from going below 1
		if (this.stats.WEAPON_CLIP_MAX < 1) {
			this.stats.WEAPON_CLIP_MAX = 1;
		}

		// Calculate battery recharge time from ammo clip size
		if (Equipment.isRechargeable(item) && this.stats.WEAPON_CLIP_MAX) {
			const regenDelay = Equipment.getRechargeDelay(item);
			const regenRate = Equipment.getRechargeRate(item);
			this.stats.WEAPON_RELOAD_TIME =
				regenDelay + this.stats.WEAPON_CLIP_MAX / regenRate;
		}

		// Modify reload time by reload speed
		if ("WEAPON_RELOAD_TIME" in this.stats && "WEAPON_RELOAD_SPEED" in this.stats) {
			const reloadTime = this.stats.WEAPON_RELOAD_TIME;
			const reloadSpeed = this.stats.WEAPON_RELOAD_SPEED;
			if (reloadSpeed > 0) {
				this.stats.WEAPON_RELOAD_TIME = reloadTime / reloadSpeed;
			}
		}

		// Calculate status chance per pellet
		if (this.stats.WEAPON_PROC_CHANCE) {
			const statusUpgradeType = upgradeTypes.WEAPON_PROC_CHANCE;
			const statusMultiplierFromMods =
				(1 + (stackingMultipliers[statusUpgradeType] || 0)) *
				(multipliers[statusUpgradeType] || 1);
			const statusChanceBeforeMultishot =
				Math.max(
					0,
					setValues[UpgradeType[statusUpgradeType]] * statusMultiplierFromMods +
						(addedValues[statusUpgradeType] || 0),
				) / baseMultishot;
			this.stats.WEAPON_PROC_CHANCE = statusChanceBeforeMultishot;

			// HACK: Artax and Cryotra status chance is multiplied by their fire rate
			if (
				this.data.item.path ===
					"/Lotus/Types/Friendly/Pets/MoaPets/MoaPetComponents/CryoxionWeapon" ||
				this.data.item.path === "/Lotus/Types/Sentinels/SentinelWeapons/Gremlin"
			) {
				const fireRateFromMods =
					(1 + (stackingMultipliers[upgradeTypes.WEAPON_FIRE_RATE] || 0)) *
					(multipliers[upgradeTypes.WEAPON_FIRE_RATE] || 1);
				this.stats.WEAPON_PROC_CHANCE *= fireRateFromMods;
				// Fire rate is hard-coded to the bonus from fire-rate mods
				this.stats.WEAPON_FIRE_RATE = fireRateFromMods;
				// Reload speed is unaffected by reload speed mods
				this.stats.WEAPON_RELOAD_TIME = setValues.WEAPON_RELOAD_TIME;
				// Magazine capacity is not affected by mods
				this.stats.WEAPON_CLIP_MAX = setValues.WEAPON_CLIP_MAX;
			}
		}

		// Cap Warframe's Ability Efficiency at 175%
		if (
			this.stats.AVATAR_ABILITY_EFFICIENCY &&
			this.stats.AVATAR_ABILITY_EFFICIENCY > 1.75
		) {
			this.stats.AVATAR_ABILITY_EFFICIENCY = 1.75;
		}

		// Ensure weapon fire rate is a valid value
		if (this.stats.WEAPON_FIRE_RATE <= 0) {
			this.stats.WEAPON_FIRE_RATE = 1;
		}

		// Calculate Charge Rate (seconds until fully charged)
		// This can be affected by stats other than Fire Rate!
		if (this.stats.WEAPON_CHARGE_RATE) {
			const chargeModifier = Equipment.getChargeModifier(
				this.data.item,
				this.data.fireModeIndex,
			);
			if (chargeModifier && this.stats[chargeModifier]) {
				const type = upgradeTypes[chargeModifier];
				// Negative fire rate appears to cap out at 10x a weapon's base speed
				this.stats.WEAPON_CHARGE_RATE =
					setValues["WEAPON_CHARGE_RATE"] /
					Math.max(
						0.1,
						this.evaluateUpgradeOperations(
							1,
							addedValues[type],
							multipliers[type],
							stackingMultipliers[type],
						),
					);
			}
		}

		// Add derived damage reduction and effective HP
		if (this.stats.AVATAR_ARMOUR) {
			this.derivedStats.damageReduction =
				this.stats.AVATAR_ARMOUR / (this.stats.AVATAR_ARMOUR + 300);
			this.derivedStats.effectiveHitPoints =
				this.stats.AVATAR_HEALTH_MAX / (1 - this.derivedStats.damageReduction) +
				this.stats.AVATAR_SHIELD_MAX;
		}
	}

	private calculateUpgradeValues() {
		const setValues = Equipment.getDefaultStats(
			this.data.item,
			this.data.fireModeIndex,
		);

		// Apply modular upgrades to base stats, then mods on top of the result
		if (Equipment.isModular(this.data.item)) {
			this.applyUpgradesToStats(setValues, this.getModularUpgrades());
		}
		this.applyUpgradesToStats(setValues, this.getPassiveUpgrades());
	}

	private getSlotsInUIOrder() {
		// Slots are displayed in reverse order from how they're stored
		return [...this.slots].reverse();
	}

	private calculateDamage() {
		if (!Equipment.isWeapon(this.data.item)) {
			return;
		}
		const damagesCategories = Equipment.getWeaponDamage(
			this.data.item,
			this.data.fireModeIndex,
			this.data.kuvaData,
		);
		this.totalDamage = 0;
		this.derivedStats.averageDamage = 0;
		this.derivedStats.burstDamage = 0;
		this.derivedStats.sustainedDamage = 0;
		const combinedDamage: {[damageType: string]: number} = {};
		for (const dt of Object.keys(this.weightedStatusChance)) {
			this.weightedStatusChance[dt] = 0;
		}
		const isMelee = Equipment.isMelee(this.data.item);
		const factionDamageMultiplier = 1 + this.getFactionDamage();
		this.splitDamage = [];
		damagesCategories.forEach(({damageSources}) => {
			damageSources.forEach(({attackData, name}) => {
				const damageKeys = Object.keys(attackData) as DamageTypeString[];

				// First, base damage is multiplied by WEAPON_DAMAGE_AMOUNT
				// Don't apply damage multiplier to projectiles
				const baseDamageUnmodded = damageKeys.reduce((a, b) => a + attackData[b], 0);

				const baseDamage =
					(isMelee ? this.stats.WEAPON_MELEE_DAMAGE : this.stats.WEAPON_DAMAGE_AMOUNT) *
					(baseDamageUnmodded + this.addedMeleeWeaponDamage) *
					factionDamageMultiplier;

				if (baseDamageUnmodded > 0) {
					damageKeys.forEach(
						(dt) => (attackData[dt] *= baseDamage / baseDamageUnmodded),
					);
				}

				const baseElementalDamage: {[key in DamageTypeString]?: number} = {};
				damageKeys
					.filter((dt) => ELEMENTAL_DAMAGE_TYPES.indexOf(dt) !== -1)
					.forEach((dt) => {
						baseElementalDamage[dt] = attackData[dt];
					});

				// Next, elemental damage is calculated and combined.
				//
				// For weapons with both a secondary and a primary elemental damage component
				// in their base damage, when a combining elemental component is added, only the
				// primary elemental component of the base damage is used to calculate the new
				// secondary component damage value; the remainder of the base damage goes toward
				// the added primary elemental.
				//
				// e.g. Lenz:
				// 660 blast, 50 impact, 10 cold
				// + 90% fire mod:
				// 594 fire, 45 fire, 9 fire
				// =>
				// (660 + 19 = 679) blast, 50 impact, (594 + 45 = 639) fire
				//
				// Otherwise, regardless of whether there is physical damage in the base, additional
				// elemental damage that is part of a base secondary's combination recipe adds to the
				// secondary damage value.
				const damageTypes = damageKeys.filter((dt) => attackData[dt] > 0);
				// Boolean to use for the Lenz case TODO
				const has = (key: DamageTypeString) => damageTypes.indexOf(key) !== -1;
				// const hasPrimaryAndSecondary =
				// 	(has("DT_EXPLOSION") && (has("DT_FIRE") || has("DT_FREEZE"))) ||
				// 	(has("DT_RADIATION") && (has("DT_FIRE") || has("DT_ELECTRICITY"))) ||
				// 	(has("DT_GAS") && (has("DT_FIRE") || has("DT_POISON"))) ||
				// 	(has("DT_MAGNETIC") && (has("DT_FREEZE") || has("DT_ELECTRICITY"))) ||
				// 	(has("DT_VIRAL") && (has("DT_FREEZE") || has("DT_POISON"))) ||
				// 	(has("DT_CORROSIVE") && (has("DT_ELECTRICITY") || has("DT_POISON")));

				// Elemental types which are a part of the base damage and should be applied after mods
				const innateElementalTypes = damageTypes
					// remove non-elemental damage types:
					.filter((dt) => ELEMENTAL_DAMAGE_TYPES.indexOf(dt) !== -1);
				// Use the order in which primary elemental damage types appear to determine which secondary elemental types to generate

				const elementalDamageTypes = this.getSlotsInUIOrder()
					.map(({elementalDamageTypes}) => elementalDamageTypes)
					.concat([innateElementalTypes])
					.reduce((a, b) => {
						b.forEach((dt) => a.indexOf(dt) === -1 && a.push(dt));
						return a;
					}, []);

				const getSecondaryType = (dtA: DamageTypeString, dtB: DamageTypeString) => {
					return SECONDARY_DAMAGE_TYPE_MAP[dtA]![dtB] || "";
				};
				const firstSecondaryType =
					elementalDamageTypes.length >= 2
						? getSecondaryType(elementalDamageTypes[0], elementalDamageTypes[1])
						: "";
				const secondSecondaryType =
					elementalDamageTypes.length >= 4
						? getSecondaryType(elementalDamageTypes[2], elementalDamageTypes[3])
						: "";

				// Remove unpaired damage types
				if (elementalDamageTypes.length === 3) {
					elementalDamageTypes.splice(2);
				} else if (elementalDamageTypes.length === 1) {
					elementalDamageTypes.splice(0);
				}

				Object.keys(this.stats)
					.filter((k) => k.indexOf("WEAPON_PERCENT_BASE_DAMAGE_ADDED") === 0)
					.forEach((k) => {
						let damageType: DamageTypeString = "DT_ANY";
						if (k.indexOf(",") !== -1) {
							damageType = k.split(",")[1] as DamageTypeString;
						}
						const m = this.stats[k];
						if (damageType === "DT_ANY") {
							Object.keys(attackData).forEach((dt) => {
								attackData[dt] += baseDamage * m;
							});
						} else {
							const elementalDamageTypeI = elementalDamageTypes.indexOf(damageType);
							if (elementalDamageTypeI >= 0) {
								attackData[
									elementalDamageTypeI < 2 ? firstSecondaryType : secondSecondaryType
								] += baseDamage * m;
							} else if (
								damageType === "DT_IMPACT" ||
								damageType === "DT_PUNCTURE" ||
								damageType === "DT_SLASH"
							) {
								// +IPS damage only affects existing IPS damage on the weapon
								if (has(damageType)) {
									attackData[damageType] *= 1 + m;
								}
							} else {
								attackData[damageType] += baseDamage * m;
							}
						}
					});

				(Object.keys(baseElementalDamage) as DamageTypeString[]).forEach((dt) => {
					const damage = baseElementalDamage[dt]!;
					const elementalDamageTypeI = elementalDamageTypes.indexOf(dt);
					if (elementalDamageTypeI >= 0) {
						attackData[dt] -= damage;
						attackData[
							elementalDamageTypeI < 2 ? firstSecondaryType : secondSecondaryType
						] += damage;
					}
				});

				// Prevent any damage type from going negative
				for (let [dt, damage] of Object.entries(attackData)) {
					if (damage < 0) {
						attackData[dt] = 0;
					}
				}

				// Add multishot multiplier to damage numbers
				const multishot = this.stats.WEAPON_FIRE_ITERATIONS || 1;
				damageKeys.forEach((dt) => {
					attackData[dt] *= multishot;
				});

				let currentDamage = damageKeys.reduce(
					(n, k) => (Number.isFinite(attackData[k]) ? n + attackData[k] : n),
					0,
				);

				damageKeys.forEach((dt) => {
					combinedDamage[dt] = (combinedDamage[dt] || 0) + attackData[dt];
				});

				this.splitDamage.push({attackData, name});

				// Calculate status chance per damage type using weighted damage
				const weightedDamage: {[damageType: string]: number} = {};
				let weightedTotalDamage = 0;
				let uniqueDamageTypes = 0;
				for (const dt of Object.keys(attackData)) {
					weightedDamage[dt] = attackData[dt];
					weightedTotalDamage += weightedDamage[dt];
					if (attackData[dt]) {
						uniqueDamageTypes += 1;
					}
				}

				// Calculate average number of active status effects for Condition Overload
				if (
					isMelee &&
					this.data.applyConditionals &&
					this.stats.WEAPON_DAMAGE_IF_VICTIM_PROC_ACTIVE > 1
				) {
					const averageActiveStatusEffects = Math.min(
						uniqueDamageTypes,
						Object.keys(weightedDamage).reduce((averageStatus, dt) => {
							// Base duration of status effect
							const effectDuration = STATUS_DURATION_MAP[dt];
							if (effectDuration) {
								// Average uptime of each status effect
								const effectCoverage =
									(weightedDamage[dt] / weightedTotalDamage) *
									this.stats.WEAPON_PROC_CHANCE *
									effectDuration *
									// Status duration multiplier from mods
									this.stats.WEAPON_PROC_TIME;
								return averageStatus + effectCoverage;
							}
							return averageStatus;
						}, 0),
					);
					const conditionOverloadMultiplier =
						1 +
						(averageActiveStatusEffects *
							(this.stats.WEAPON_DAMAGE_IF_VICTIM_PROC_ACTIVE - 1)) /
							this.stats.WEAPON_MELEE_DAMAGE;

					// Apply Condition Overload damage to total
					currentDamage *= conditionOverloadMultiplier;

					// Apply Condition Overload to each damage type
					Object.keys(combinedDamage).forEach((dt) => {
						combinedDamage[dt] *= conditionOverloadMultiplier;
					});
				}

				// Add this damage source to weapon's overall damage
				this.totalDamage += currentDamage;

				// WEAPON_PROC_CHANCE is probability at least one pellet will trigger a status effect
				// Average status chance per pellet = 1 - (1 - displayed status chance) ^ (1 / pellet count)
				const fireIterations = Equipment.hasCompatibilityTag(this.data.item, "BEAM")
					? this.derivedStats.baseMultishot
					: this.stats.WEAPON_FIRE_ITERATIONS;
				const statusChancePerPellet = this.stats.WEAPON_PROC_CHANCE;
				const statusChancePerShot = statusChancePerPellet * fireIterations;
				this.derivedStats.statusChanceAfterMultishot = statusChancePerShot;
				for (const dt of Object.keys(attackData)) {
					this.weightedStatusChance[dt] =
						(this.weightedStatusChance[dt] || 0) +
						(weightedDamage[dt] / weightedTotalDamage) * statusChancePerShot;
				}

				// Prevent crit chance from going negative
				if (this.stats.WEAPON_CRIT_CHANCE < 0) {
					this.stats.WEAPON_CRIT_CHANCE = 0;
				}

				// Calculate DPS stats
				const fireRate = this.stats.WEAPON_FIRE_RATE || 0;
				const critChance = this.stats.WEAPON_CRIT_CHANCE || 0;
				const critDamage = this.stats.WEAPON_CRIT_DAMAGE || 0;
				const doubleCritChance = critChance * this.stats.WEAPON_DOUBLE_CRIT_CHANCE || 0;
				const fireBehavior = Equipment.getWeaponFireBehavior(
					this.data.item,
					this.data.fireModeIndex,
				);
				const ammoRequirement =
					fireBehavior && fireBehavior.UseAmmo && fireBehavior.ammoRequirement
						? fireBehavior.ammoRequirement
						: 1;
				const magazineSize =
					Math.max(1, Math.round(this.stats.WEAPON_CLIP_MAX || 0)) / ammoRequirement;
				const averageDamage =
					(currentDamage * (1 + critChance * (critDamage - 1)) +
						doubleCritChance * currentDamage) *
					// Primed Chamber increasing damage of first shot in magazine
					(1 + ((this.stats.WEAPON_INIT_DAMAGE_MOD || 1) - 1) / magazineSize);

				const reloadSpeed = this.stats.WEAPON_RELOAD_TIME || 1;

				this.derivedStats.averageDamage += averageDamage;
				if (isMelee) {
					this.derivedStats.sustainedDamage += averageDamage * fireRate;
				} else {
					const chargeOrFireRate = this.stats.WEAPON_CHARGE_RATE
						? 1 / this.stats.WEAPON_CHARGE_RATE
						: fireRate;

					// Burst Damage (average DPS without reloading)
					// Shouldn't exceed total damage of 1 magazine
					this.derivedStats.burstDamage +=
						magazineSize < chargeOrFireRate
							? averageDamage * magazineSize
							: averageDamage * chargeOrFireRate;

					// Sustained Damage (total damage, crit, fire rate, magazine size, reload speed)
					this.derivedStats.sustainedDamage +=
						averageDamage *
						chargeOrFireRate *
						(magazineSize / (chargeOrFireRate * reloadSpeed + magazineSize));

					// Hunter Munitions mod
					// On crit, instantly deal 35% baseDamage, followed by 6 ticks over 6 seconds
					if (this.stats.WEAPON_SLASH_PROC_ON_CRIT_CHANCE) {
						const bleedMultiplier = 0.35; // Bleed for 35% of base damage
						const burstTickCount = 2;
						const sustainedTickCount = 7;
						const bleedTickDamage =
							(baseDamage + doubleCritChance * baseDamage) *
							bleedMultiplier *
							fireIterations *
							critChance *
							critDamage *
							this.stats.WEAPON_SLASH_PROC_ON_CRIT_CHANCE *
							factionDamageMultiplier *
							// Primed Chamber
							(1 + ((this.stats.WEAPON_INIT_DAMAGE_MOD || 1) - 1) / magazineSize);

						// Give Burst damage 2 ticks (instant + 1 second later)
						const bleedBurstDamage =
							magazineSize < chargeOrFireRate
								? bleedTickDamage * magazineSize
								: bleedTickDamage * chargeOrFireRate;
						this.derivedStats.burstDamage += bleedBurstDamage * burstTickCount;

						// Give Sustained damage all 7 ticks
						this.derivedStats.sustainedDamage +=
							bleedTickDamage *
							chargeOrFireRate *
							(magazineSize / (chargeOrFireRate * reloadSpeed + magazineSize)) *
							sustainedTickCount;
					}

					// Hack to replace Mesa Regulators Sustained DPS with Burst because they don't reload
					if (
						this.data.item.path === "/Lotus/Powersuits/Cowgirl/SlingerPistols" ||
						this.data.item.path === "/Lotus/Powersuits/Cowgirl/PrimeSlingerPistols"
					) {
						this.derivedStats.sustainedDamage = this.derivedStats.burstDamage;
					}
				}
			});
		});
		// todo: This should be displayed as separate damage sources
		// For now we'll combine impact, explosion, etc into one
		this.damage = combinedDamage;
	}
}
