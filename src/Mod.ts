import {RivenSlotData} from "./Build";
import {
	ArtifactPolarity,
	ELEMENTAL_DAMAGE_TYPES,
	ModUsability,
	ProductCategory,
	Quota,
	Rarity,
	SlotPolarity,
	SlotType,
} from "./enums";
import Equipment from "./Equipment";
import {
	EquipmentData,
	Item,
	LocTag,
	ModData,
	PackagePath,
	RivenData,
	UpgradeData,
} from "./interfaces";
import Riven from "./Riven";
import Scripts from "./Scripts";

type FormatArgs = string[] | {[key: string]: string};

const asPercent = (i: number, roundTo: number) =>
	(roundTo === 0.1 ? Math.round(i * 1000) / 10 : Math.round(i * 100)).toString();
const roundValue = (value: number, roundTo?: number) =>
	roundTo === 0.1 ? Number(value.toFixed(1)).toString() : value.toFixed(0);
const formatUpgradeValue = (element: UpgradeData, value: number) => {
	const finalValue = element.ReverseValueSymbol ? value * -1 : value;
	const symbol = finalValue >= 0 ? "+" : "";
	return element.DisplayAsPercent === 1
		? `${symbol}${asPercent(finalValue, element.RoundTo)}`
		: `${symbol}${roundValue(finalValue, element.RoundTo)}`;
};

const getSyndicatePowerTag = (tag: string) => {
	switch (tag) {
		case "/Lotus/Syndicates/CephalonSudaSyndicate":
			return "/Lotus/Language/Syndicates/CephalonPower";
		case "/Lotus/Syndicates/ArbitersSyndicate":
			return "/Lotus/Language/Syndicates/ArbitersPower";
		case "/Lotus/Syndicates/SteelMeridianSyndicate":
			return "/Lotus/Language/Syndicates/MeridianPower";
		case "/Lotus/Syndicates/NewLokaSyndicate":
			return "/Lotus/Language/Syndicates/NewLokaPower";
		case "/Lotus/Syndicates/PerrinSyndicate":
			return "/Lotus/Language/Syndicates/PerrinPower";
		case "/Lotus/Syndicates/RedVeilSyndicate":
		default:
			return "/Lotus/Language/Syndicates/RedVeilPower";
	}
};

export default class Mod {
	public static isArcane(mod: Item<ModData>) {
		return mod.data.SearchTags
			? mod.data.SearchTags[0] === "/Lotus/Language/Categories/ENHANCEMENTS"
			: false;
	}

	public static isAmalgam(mod: Item<ModData>) {
		return !!mod.data.IsAmalgam;
	}

	public static isAura(mod: Item<ModData>) {
		return !!mod.data.TargetMode || !!mod.data.TargetType;
	}

	public static isExilus(mod: Item<ModData>) {
		return mod.data.IsUtility === 1;
	}

	public static isImmortal(mod: Item<ModData>) {
		return !!mod.data.IsImmortal;
	}

	public static isRiven(path: PackagePath) {
		return path.startsWith("/Lotus/Upgrades/Mods/Randomized/");
	}

	public static isStance(mod: Item<ModData>) {
		return !!mod.data.Finishers;
	}

	public static isSentinel(mod: Item<ModData>) {
		const compat = mod.data.ItemCompatibility;
		return (
			compat === "/Lotus/Types/Sentinels/SentinelPowerSuit" ||
			compat === "/Lotus/Types/Game/Pets/RoboticPetPowerSuit"
		);
	}

	public static isFlawed(mod: Item<ModData>) {
		return !!mod.data.IsStarter;
	}

	public static isAvionic(mod: Item<ModData>) {
		return Equipment.isA(mod, "/Lotus/Types/Game/CrewShip/CrewShipUpgrade");
	}

	public static isSetMod(mod: Item<ModData>) {
		return !!mod.data.ModSet;
	}

	public static hasPositiveDrain(mod: Item<ModData>) {
		return Mod.isAura(mod) || Mod.isStance(mod);
	}

	public static hasRequiredCompatibilityTag(
		mod: Item<ModData>,
		item: Item<EquipmentData>,
	) {
		if (!mod.data.CompatibilityTags) return true;
		for (const tag of mod.data.CompatibilityTags || []) {
			if (Equipment.hasCompatibilityTag(item, tag)) {
				return true;
			}
		}
		return false;
	}

	public static isIncompatible(mod: Item<ModData>, item: Item<EquipmentData>) {
		for (const tag of mod.data.IncompatibilityTags || []) {
			if (Equipment.hasCompatibilityTag(item, tag)) {
				return true;
			}
		}
		return false;
	}

	public static getCompatibilityTagOverride = (
		mod: Item<ModData>,
	): LocTag | undefined => {
		if (mod.data.ItemCompatibilityLocOverride) {
			return mod.data.ItemCompatibilityLocOverride;
		} else if (Mod.isAura(mod)) {
			return "/Lotus/Language/Menu/CategoryAura";
		}
	};

	public static getModSlotType(mod: Item<ModData>) {
		return Mod.isAura(mod)
			? SlotType.AURA
			: Mod.isExilus(mod)
			? SlotType.EXILUS
			: Mod.isStance(mod)
			? SlotType.STANCE
			: Mod.isArcane(mod)
			? SlotType.ARCANE
			: SlotType.NORMAL;
	}

	public static isCompatibleWithSlot(mod: Item<ModData>, slotType: SlotType) {
		const modSlotType = Mod.getModSlotType(mod);
		// Mod and slot type match
		if (modSlotType === slotType) {
			return true;
		}
		// Exilus can be placed in normal slots
		if (Mod.isExilus(mod) && slotType === SlotType.NORMAL) {
			return true;
		}
		return false;
	}

	public static getPolarity(mod: Item<ModData>, rivenData?: RivenSlotData) {
		if (rivenData && rivenData.polarity) {
			return rivenData.polarity;
		}
		if (!mod.data.ArtifactPolarity) {
			return ArtifactPolarity.AP_UNIVERSAL;
		}
		return ArtifactPolarity[mod.data.ArtifactPolarity];
	}

	public static getRarity(mod: Item<ModData>) {
		if (!mod.data.Rarity) {
			return Rarity.COMMON;
		}
		return Rarity[mod.data.Rarity];
	}

	public static getMaxRank(mod: Item<ModData>) {
		// Immortal mods shouldn't display ranks
		if (Mod.isImmortal(mod)) {
			return 0;
		}
		// Stances default to a max of 3 ranks instead of 5
		return {
			[Quota.QA_NONE]: 0,
			[Quota.QA_LOW]: 2,
			[Quota.QA_MEDIUM]: 3,
			[Quota.QA_HIGH]: 5,
			[Quota.QA_VERY_HIGH]: 10,
		}[Quota[mod.data.FusionLimit || (Mod.isStance(mod) ? "QA_MEDIUM" : "QA_HIGH")]];
	}

	public static getBaseDrain(mod: Item<ModData>) {
		return {
			[Quota.QA_NONE]: 0,
			[Quota.QA_LOW]: 2,
			[Quota.QA_MEDIUM]: 4,
			[Quota.QA_HIGH]: 6,
			[Quota.QA_VERY_HIGH]: 10,
		}[Quota[mod.data.BaseDrain || "QA_LOW"]];
	}

	public static getPolarityMatch(
		slotPolarity: ArtifactPolarity,
		modPolarity: ArtifactPolarity,
	) {
		if (slotPolarity === ArtifactPolarity.AP_ANY) {
			return SlotPolarity.MATCH;
		}
		if (
			slotPolarity === ArtifactPolarity.AP_UNIVERSAL ||
			modPolarity === ArtifactPolarity.AP_UNIVERSAL
		) {
			return SlotPolarity.NEUTRAL;
		}
		if (slotPolarity === modPolarity) {
			return SlotPolarity.MATCH;
		}
		return SlotPolarity.MISMATCH;
	}

	public static getPolarityMultiplier(
		mod: Item<ModData>,
		slotPolarity: ArtifactPolarity,
		rivenData?: RivenSlotData,
	) {
		const modPolarity = Mod.getPolarity(mod, rivenData);
		const slotPolarityMatch = Mod.getPolarityMatch(slotPolarity, modPolarity);
		if (slotPolarityMatch === SlotPolarity.NEUTRAL) {
			return 1;
		}
		if (slotPolarityMatch === SlotPolarity.MATCH) {
			return Mod.hasPositiveDrain(mod) ? 2.0 : 0.5;
		}
		return Mod.hasPositiveDrain(mod) ? 0.75 : 1.25;
	}

	public static getUsability(mod: Item<ModData>) {
		const pve =
			mod.data.AvailableOnPve !== undefined ? !!mod.data.AvailableOnPve : true;
		const pvp = !!mod.data.AvailableOnPvp;
		if (pve && pvp) {
			return ModUsability.UNIVERSAL;
		} else if (pvp) {
			return ModUsability.PVP_ONLY;
		} else if (pve) {
			return ModUsability.PVE_ONLY;
		}
		return ModUsability.INVALID;
	}

	public static getDrain(
		mod: Item<ModData>,
		rank: number,
		slotPolarity: ArtifactPolarity,
		rivenData?: RivenSlotData,
	) {
		if (Mod.getModSlotType(mod) === SlotType.ARCANE) {
			return 0;
		}
		const baseDrain = rank + Mod.getBaseDrain(mod);
		// Aura mods add capacity instead of using it:
		const drain = Math.round(
			Mod.getPolarityMultiplier(mod, slotPolarity, rivenData) * baseDrain,
		);
		return Mod.hasPositiveDrain(mod) ? -drain : drain;
	}

	public static getUpgradeDescriptionData(
		upgrade: UpgradeData,
		rank: number,
		upgrades: UpgradeData[],
		modSetMultiplier?: number,
	): [string, FormatArgs | null, boolean] | void {
		if (upgrade.OverrideLocalization) {
			// Handle locscript override
			if (upgrade.LocTag && upgrade.LocKeyWordScript) {
				if (upgrade.LocKeyWordScript.Function) {
					const ref = `${upgrade.LocKeyWordScript.Script}:${
						upgrade.LocKeyWordScript.Function
					}`;
					const script = Scripts[ref];
					if (script) {
						const args = script(
							rank,
							upgrade.LocKeyWordScript,
							upgrade.RoundTo,
							upgrades,
						);
						return [upgrade.LocTag, args, false];
					} else {
						return [upgrade.LocTag, ["?"], false];
					}
				} else {
					return [
						upgrade.LocTag,
						{
							val: formatUpgradeValue(
								upgrade,
								upgrade.Value * (rank + 1) * (modSetMultiplier || 1),
							),
						},
						false,
					];
				}
			}
		} else if (upgrade.UpgradeType) {
			const value = upgrade.Value * (rank + 1);
			if (upgrade.UpgradeType === "WEAPON_SYNDICATE_POWER") {
				return [getSyndicatePowerTag(upgrade.UpgradeObject), {val: `+${value}`}, false];
			} else {
				return [
					"|val|% |tag|",
					{
						val: formatUpgradeValue(
							upgrade,
							upgrade.OperationType === "STACKING_MULTIPLY" ? value * 100 : value,
						),
						tag: Equipment.getUpgradeStatTag(upgrade.UpgradeType),
					},
					true,
				];
			}
		}
	}

	/**
	 * Returns a list of tuple data that describe the mod's description.
	 * The tuple looks like:
	 * - `tag`: A LocTag (or a raw string if overrideLocalization is true)
	 * - `args`: Arguments to format the final string with (array or object)
	 * - `overrideLocalization`: If true, `tag` is a raw string, not a LocTag.
	 * @param mod The mod to check.
	 * @param rank The mod's expected rank (changes the values).
	 */
	public static getDescriptionData(
		mod: Item<ModData>,
		rank: number,
		modSetNumEquipped?: number,
	) {
		const ret: Array<[string, FormatArgs | null, boolean]> = [];
		if (mod.data.LocalizeDescTag && !mod.data.ModSet) {
			if (
				!mod.data.Upgrades ||
				!mod.data.Upgrades.find(
					(upgrade) =>
						!!upgrade.OverrideLocalization &&
						upgrade.LocTag === mod.data.LocalizeDescTag,
				)
			) {
				ret.push([mod.data.LocalizeDescTag, null, false]);
			}
		}
		const modSetMultiplier = this.getModSetMultiplier(mod, modSetNumEquipped);
		if (mod.data.SubUpgrade) {
			const data = Mod.getDescriptionData(
				{...mod, data: mod.data.SubUpgrade},
				rank,
				modSetNumEquipped,
			);
			if (data) {
				ret.push(...data);
			}
		}
		if (
			mod.data.Upgrades &&
			(mod.data.PrependSubUpgradeLoc || !mod.data.SubUpgrade || !ret.length)
		) {
			for (const upgrade of mod.data.Upgrades) {
				const data = Mod.getUpgradeDescriptionData(
					upgrade,
					rank,
					mod.data.Upgrades,
					modSetMultiplier,
				);
				if (data) {
					ret.push(data);
				}
			}
		}
		return ret;
	}

	public static getEnhancementData(mod: Item<ModData>, rank: number) {
		const ret: {
			values: Array<[string, string, boolean]>;
			upgrades: Array<[string, FormatArgs | null, boolean]>;
		} = {values: [], upgrades: []};
		if (mod.data.ConditionTag) {
			ret.values.push([mod.data.ConditionTag, "CONDITION", false]);
		}
		if (mod.data.UpgradeChance) {
			// UpgradeChance value of 0 indicates it is not used
			if (mod.data.UpgradeChance > 0) {
				ret.values.push([
					`${roundValue(mod.data.UpgradeChance * 100 * (rank + 1), 1)}`,
					"CHANCE",
					true,
				]);
			}
		} else {
			// seems to default to 10% per rank if UpgradeChance is missing
			ret.values.push([`${10 * (rank + 1)}`, "CHANCE", true]);
		}
		if (mod.data.UpgradeDuration && mod.data.UpgradeDuration > 0) {
			ret.values.push([
				`${(mod.data.UpgradeDuration * (rank + 1)).toFixed(1)}`,
				"DURATION",
				true,
			]);
		}

		if (!mod.data.Upgrades) {
			return ret;
		}

		mod.data.Upgrades.forEach((upgrade) => {
			const data = Mod.getUpgradeDescriptionData(upgrade, rank, mod.data.Upgrades!);
			if (data) {
				ret.upgrades.push(data);
			}
		});
		return ret;
	}

	// We subtract 2 from numEquipped to match index for ModSetValues,
	// which starts from 0 with the multiplier for having 2 mods equipped
	public static getModSetMultiplier(mod: Item<ModData>, numEquipped?: number) {
		if (
			numEquipped &&
			numEquipped > 1 &&
			mod.data.ModSetValues &&
			numEquipped <= mod.data.ModSetValues.length + 2
		) {
			return 1 + mod.data.ModSetValues[numEquipped - 2];
		}
		return 1;
	}

	public static getElementalDamageTypes(
		mod: Item<ModData>,
		applyConditionals?: boolean,
		rivenData?: RivenSlotData,
	) {
		// Handle riven elements by reading from the bottom up
		if (rivenData) {
			return Array.from(Riven.getPassiveUpgrades(mod as Item<RivenData>, rivenData, 1))
				.reverse()
				.map((u) => u.DamageType)
				.filter((dt) => ELEMENTAL_DAMAGE_TYPES.indexOf(dt) !== -1);
		}
		if (!mod.data.Upgrades || (mod.data.ConditionalUpgrades && !applyConditionals)) {
			return [];
		}
		return mod.data.Upgrades.map((u) => u.DamageType).filter(
			(dt) => ELEMENTAL_DAMAGE_TYPES.indexOf(dt) !== -1,
		);
	}

	// Returns an iterator of upgrades which are active
	public static *getPassiveUpgrades(
		mod: Item<ModData>,
		applyConditionals?: boolean,
	): IterableIterator<UpgradeData> {
		if (!applyConditionals && mod.data.ConditionalUpgrades) {
			return;
		}
		// Aura mods must target avatars
		if (
			mod.data.TargetType !== undefined &&
			mod.data.TargetType !== "/Lotus/Types/Player/TennoAvatar"
		) {
			return;
		}
		// SubUpgrades (Mostly used for Avionics)
		if (mod.data.SubUpgrade) {
			for (const upgrade of Mod.getPassiveUpgrades(
				{...mod, data: mod.data.SubUpgrade},
				applyConditionals,
			)) {
				yield upgrade;
			}
		}

		if (!mod.data.Upgrades) {
			return;
		}
		// Treat Blood Rush, Split Flights, etc as if they have max potential stacks
		if (mod.data.MaxConditionalStacks) {
			if (applyConditionals) {
				for (let i = 0; i < mod.data.MaxConditionalStacks; i++) {
					for (const upgrade of mod.data.Upgrades) {
						// Ignore "SymbolFilter": "CC_SLIDING_PVP" making Maiming Strike count twice
						if (upgrade.SymbolFilter.includes("PVP")) {
							continue;
						}
						if (!applyConditionals) {
							if (
								upgrade.SymbolFilter &&
								upgrade.UpgradeType !== "GAMEPLAY_FACTION_DAMAGE"
							) {
								continue;
							}
							// Ignore "heavy attack" bonus for Sacrificial Steel unless "apply conditionals" is active
							if (upgrade.ValidModifiers.length) {
								continue;
							}
						}
						yield upgrade;
					}
				}
			}
			return;
		}
		for (const upgrade of mod.data.Upgrades) {
			// Ignore "SymbolFilter": "CC_SLIDING_PVP" making Maiming Strike count twice
			if (upgrade.SymbolFilter.includes("PVP")) {
				continue;
			}
			if (!applyConditionals) {
				if (upgrade.SymbolFilter && upgrade.UpgradeType !== "GAMEPLAY_FACTION_DAMAGE") {
					continue;
				}
				// Ignore "heavy attack" bonus for Sacrificial Steel unless "apply conditionals" is active
				if (upgrade.ValidModifiers.length) {
					continue;
				}
			}
			yield upgrade;
		}
	}

	public static isCompatibleWith(
		mod: Item<ModData>,
		item: Item<EquipmentData>,
		helminthAbility?: PackagePath,
	) {
		if (Mod.isIncompatible(mod, item)) {
			return false;
		}

		if (!Mod.hasRequiredCompatibilityTag(mod, item)) {
			return false;
		}

		if (mod.data.ItemCompatibility && Equipment.isA(item, mod.data.ItemCompatibility)) {
			return true;
		}

		// Zaw arcanes have "/Lotus/Weapons/Ostron/Melee/LotusModularWeapon" item compatibility tag but that tag is not included in their parents array.
		if (
			Equipment.isZaw(item) &&
			(mod.data.ItemCompatibility ===
				"/Lotus/Weapons/Ostron/Melee/LotusModularWeapon" ||
				(item.data.MeleeStyle &&
					mod.data.ModularWeaponMeleeStyleCompatibility === item.data.MeleeStyle))
		) {
			return true;
		}

		// Kitgun arcanes have a similar issue as zaw arcanes
		if (
			Equipment.isKitgun(item) &&
			mod.data.ItemCompatibility === "/Lotus/Weapons/Tenno/LotusBulletWeapon"
		) {
			return true;
		}

		if (
			mod.data.ItemCompatibility === "/Lotus/Weapons/Tenno/Pistol/LotusPistol" &&
			Equipment.isA(item, "/Lotus/Types/Weapon/LotusCustomAimWeapon")
		) {
			// Hardcoded check for Regulators Prime
			return true;
		}

		if (Mod.isSentinel(mod)) {
			return item.data.ProductCategory === ProductCategory.Sentinels;
		}

		// Allow augments for helminth abilities to be equipped
		if (
			helminthAbility &&
			mod.data.IsHelminthAugment &&
			mod.data.Upgrades &&
			mod.data.Upgrades.find((upgrade) => upgrade.UpgradeObject === helminthAbility)
		) {
			return true;
		}

		return false;
	}
}
