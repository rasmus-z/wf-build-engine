import ItemsJSON from "./items.json";
import ModsJSON from "./mods.json";

const Mods = ModsJSON as any;
const Items = ItemsJSON as any;

/**
 * Mods
 */

export const Adaptation =
	Mods["/Lotus/Upgrades/Mods/Warframe/AvatarResistanceOnDamageMod"];
export const AmalgamBarrelDiffusion =
	Mods["/Lotus/Upgrades/Mods/DualSource/Pistol/MultishotDodgeMod"];
export const AnimalInstinct =
	Mods["/Lotus/Upgrades/Mods/Sentinel/SentinelLootRadarEnemyRadarMod"];
export const ArcaneAcceleration =
	Mods["/Lotus/Upgrades/CosmeticEnhancers/Offensive/LongGunSpeedOnCrit"];
export const AssaultMode =
	Mods["/Lotus/Types/Sentinels/SentinelPrecepts/SentinelAttack"];
export const CorrosiveProjection =
	Mods["/Lotus/Upgrades/Mods/Aura/EnemyArmorReductionAuraMod"];
export const FatalAttraction = Mods["/Lotus/Types/Sentinels/SentinelPrecepts/Bait"];
export const PrimedRegen = Mods["/Lotus/Types/Sentinels/SentinelPrecepts/PrimedRegen"];
export const BlindJustice =
	Mods["/Lotus/Weapons/Tenno/Melee/MeleeTrees/KatanaCmbThreeMeleeTree"];
export const MaimingStrike =
	Mods["/Lotus/Upgrades/Mods/Melee/Event/SlideAttackCritChanceMod"];
export const SteelCharge = Mods["/Lotus/Upgrades/Mods/Aura/PlayerMeleeAuraMod"];
export const UmbralVitality = Mods["/Lotus/Upgrades/Mods/Sets/Umbra/WarframeUmbraModA"];

/**
 * Items
 */

export const Ash = Items["/Lotus/Powersuits/Ninja/Ninja"];
export const Djinn = Items["/Lotus/Types/Sentinels/SentinelPowersuits/GubberPowerSuit"];
export const Vulklok = Items["/Lotus/Types/Sentinels/SentinelWeapons/SentElecRailgun"];
export const ChesaKubrow =
	Items["/Lotus/Types/Game/KubrowPet/RetrieverKubrowPetPowerSuit"];
