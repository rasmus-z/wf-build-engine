import Riven from "../Riven";

test("Riven.isRiven()", () => {
	expect(Riven.isRiven("")).toBe(false);
	expect(Riven.isRiven("/Lotus/Powersuits/AntiMatter/Anti")).toBe(false);
	expect(Riven.isRiven("/Lotus/Powersuits/AntiMatter/AntiMatterDropAugmentCard")).toBe(
		false,
	);
	expect(
		Riven.isRiven("/Lotus/Upgrades/Mods/Randomized/LotusArchgunRandomModRare"),
	).toBe(true);
});
