import fs from "fs";
import https from "https";
import path from "path";

const baseUrl = "https://gitlab.com/magicfind/warframe/warframejson/raw/master/";

const downloadFile = async (filename: string) => {
	const filePath = path.resolve(__dirname, filename);
	const url = baseUrl + filename;

	if (fs.existsSync(filePath)) {
		return;
	}

	const file = fs.createWriteStream(filePath);

	return await https.get(url, (res) => {
		if (res.statusCode !== 200) {
			throw new Error(`Got status code ${res.statusCode} downloading ${url}`);
		}
		res.pipe(file);
	});
};

export default () => {
	["items.json", "mods.json", "modsets.json"].forEach(async (filename) => {
		await downloadFile(filename);
	});
};
