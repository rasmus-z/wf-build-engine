import {
	ArtifactPolarity,
	ComponentCategory,
	DamageHitType,
	DamageType,
	InventorySlot,
	ItemType,
	KitgunBarrelType,
	KitgunGunType,
	MarketMode,
	ModularPartType,
	OperationType,
	Posture,
	PostureModifier,
	ProductCategory,
	Quota,
	Rarity,
	RelicQuality,
	UpgradeType,
} from "./enums";

export type LocTag = string;
export type PackagePath = string;
export type ItemPath = PackagePath;
export type CompatibilityTag = string;

type DEBoolean = 0 | 1;

export interface Item<T> {
	data: T;
	id: number;
	path: ItemPath;
	parent?: ItemPath;
	parents: ItemPath[];
	storeData?: ItemStoreData;
	storeItemType?: PackagePath;
	texture?: string;
	tag?: ItemType;
}

export interface ItemDB<T> {
	[key: string]: Item<T>;
}

export interface ScriptData {
	Function?: string;
	Script: PackagePath | "";
	[key: string]: any;
}

interface PVPValueData {
	DebugPVPValue: number;
	Range: number[];
	RoundTo?: number;
}

export interface BaseUpgradeData {
	DisplayAsPercent?: DEBoolean;
	LocKeyWordScript?: ScriptData;
	LocTag: LocTag | "";
	OverrideLocalization: DEBoolean;
	ReverseValueSymbol?: DEBoolean;
	RoundTo: number;
}

export interface UpgradeData extends BaseUpgradeData {
	AutoType: DEBoolean;
	DamageType: keyof typeof DamageType;
	OperationType: keyof typeof OperationType;
	SymbolFilter: string;
	UpgradeObject: PackagePath | "";
	UpgradeType: keyof typeof UpgradeType;
	ValidModifiers: Array<keyof typeof PostureModifier>;
	ValidPostures: Array<keyof typeof Posture>;
	ValidProcTypes: string[]; // PT_…
	ValidType: ItemPath | "";
	Value: number;
}

export interface InterfaceDescriptionData {
	CodexSecret?: DEBoolean;
	CompatibilityTags?: CompatibilityTag[];
	ExcludeFromCodex?: DEBoolean;
	IconTexture?: PackagePath; // Path to texture
	IncompatibilityTags?: CompatibilityTag[];
	LocalizeDescTag: LocTag | ""; // Path to description loc name
	LocalizeTag: LocTag | ""; // Path to loc name
	MarketMode?: MarketMode;
	ProductCategory?: ProductCategory; // Item type
}

export interface ItemMaterialData extends InterfaceDescriptionData {
	ComponentCategory?: ComponentCategory;
	Equipable?: DEBoolean;
	MarketDescriptionOverride?: LocTag;
	PurchaseQuantity?: number;
	SortingPriority?: number;
}

export interface ItemIngredients {
	ItemCount: number;
	ItemType: PackagePath;
}

export interface ItemRecipeData extends EquipmentData {
	Amount?: [number, number];
	BuildPrice?: number;
	BuildTime?: number;
	DestroyAfterCollecting?: DEBoolean;
	Ingredients?: ItemIngredients[];
	ResearchIngredients?: ItemIngredients[];
	MarketMode?: MarketMode;
	ProductCategory?: ProductCategory;
	ReplicateTechPrice?: number;
	ResearchRegularPrice?: number;
	ResearchTime?: number;
	ResultItem: PackagePath;
	SkipBuildTimePrice?: number;
	SkipResearchTimePrice?: number;
	StoreItemSpecialization?: PackagePath;
	TechPrereqItemType?: PackagePath;
	UseAlternateItemDisplay?: DEBoolean;
}

export interface ItemStoreData {
	DisplayRecipe?: PackagePath;
	PremiumPrice?: number; // plat cost from market
	ProductCategory?: string;
	RecipeHelpText?: LocTag;
	RegularPrice?: number; // credit cost from market
	SearchTags?: LocTag[];
	SellingPrice?: number; // credits from selling item
	ShowInMarket?: DEBoolean;
}

export interface ModData extends InterfaceDescriptionData {
	ApplyUpgradesByDefault?: DEBoolean; // default true
	ArtifactPolarity?: keyof typeof ArtifactPolarity;
	AvailableOnPve?: DEBoolean; // default false
	AvailableOnPvp?: DEBoolean; // default true
	BaseDrain?: keyof typeof Quota;
	ConditionalUpgrades?: ItemPath[];
	ConditionTag?: LocTag;
	EnhancementTag?: LocTag;
	Finishers?: PackagePath;
	FusionLimit?: keyof typeof Quota;
	IsAbilityAugment?: DEBoolean;
	IsAmalgam?: DEBoolean;
	IsHelminthAugment?: DEBoolean;
	IsImmortal?: DEBoolean; // Requiem/Parazon Mods
	IsStarter?: DEBoolean;
	IsTransmutationCore?: DEBoolean;
	IsUtility?: DEBoolean;
	ItemCompatibility?: ItemPath;
	ItemCompatibilityLocOverride?: LocTag;
	MaxConditionalStacks?: number;
	ModSet?: PackagePath;
	ModSetData?: ModSetData;
	ModSetValues?: number[];
	ModularWeaponMeleeStyleCompatibility?: string;
	PremiumPrice?: number;
	PrependSubUpgradeLoc?: DEBoolean;
	Rarity?: keyof typeof Rarity;
	RegularPrice?: number;
	SearchTags?: LocTag[];
	SellingPrice?: number;
	SubUpgrade?: ModData;
	TargetMode?: string; // e.g. TM_FACTION
	TargetType?: string; // Path
	UpgradeChance?: number;
	UpgradeDuration?: number;
	Upgrades?: UpgradeData[];
}

export interface AttackData {
	Amount?: number;
	DT_CORROSIVE?: number;
	DT_ELECTRICITY?: number;
	DT_EXPLOSION?: number;
	DT_FIRE?: number;
	DT_FREEZE?: number;
	DT_GAS?: number;
	DT_IMPACT?: number;
	DT_MAGNETIC?: number;
	DT_POISON?: number;
	DT_PUNCTURE?: number;
	DT_RADIANT?: number;
	DT_RADIATION?: number;
	DT_SLASH?: number;
	DT_VIRAL?: number;
	HitType?: keyof typeof DamageHitType;
	ProcChance?: number;
	Type?: keyof typeof DamageType;
	UseNewFormat?: DEBoolean; // implies Amount and Type are ignored
}

export interface ProjectileTypeData {
	AttackData?: AttackData;
	BasePunctureDepth?: number;
	BounceOnAvatars?: DEBoolean; // Explodes on contact or passes through units
	CanStick?: DEBoolean; // Compatible with Adhesive Blast mod
	ClusterProjectiles?: ProjectileTypeData;
	ContactExplosionDamage?: number; // Doesn't contribute to displayed damage, unclear how this works
	ContactExplosionDamageType?: keyof typeof DamageType;
	ContactExplosionRadius?: number;
	CriticalChance?: number;
	CriticalMultiplier?: number;
	DamageRadius?: number; // ExplosiveAttack
	DealDamageThroughImpactBehavior?: DEBoolean; // ??
	DealExplosionDamageOnImpact?: DEBoolean;
	Embed?: DEBoolean;
	EmbedAttack?: AttackData;
	EmbedDamageRadius?: number; // EmbedAttack
	EmbedDeathAttack?: AttackData;
	EmbedTime?: [number, number];
	ExplodeOnFriendlies?: DEBoolean;
	ExplodeOnImpact?: DEBoolean;
	ExplosionIgnoreSource?: DEBoolean;
	ExplosiveAttack?: AttackData;
	// KinematicInitialSpeed?: number;
	// KinematicMaxSpeed?: number;
	// KinematicMovement?: DEBoolean; // todo: Document projectile flight behavior
	MaxBounces?: number; // Number of surfaces to bounce off before exploding on impact
	MaxLife?: number; // Seconds until projectile expires/explodes on its own
	NumClusterProjectiles?: number;
	OwnEmbedEffect?: DEBoolean; // indicates Embed should be ignored (?)
	SilentProjectile?: DEBoolean;
}

export interface MeleeProxies {
	MAIN_HAND?: {
		ExtraSweepDistance?: number;
		LocalPosition: [number, number, number];
	};
}

export interface WeaponFireBehavior {
	AIMED_ACCURACY?: {
		Spread?: {
			SHOOTING?: {
				range?: [number, number];
			};
		};
	};
	AmmoConsumptionDivisor?: number;
	ammoRequirement?: DEBoolean;
	ammoType?: string; // path e.g. /Lotus/Weapons/Ammo/SniperAmmoEx
	CentralBeam?: DEBoolean;
	chargedProjectileSequenceList?: ProjectileTypeData[];
	chargedProjectileType?: ProjectileTypeData;
	DefaultMeleeProxies?: MeleeProxies;
	fireIterations?: number;
	IgnoreFireIterations?: DEBoolean;
	IsMeleeBehavior?: DEBoolean;
	IsSilenced?: DEBoolean;
	MultiBeam?: DEBoolean;
	projectileSequenceList?: ProjectileTypeData[];
	projectileType?: ProjectileTypeData;
	RoundUpAmmoConsumption?: DEBoolean;
	ScaleAmmoRequirement?: DEBoolean;
	SweepRadius?: number;
	tracePunctureDepth?: number;
	traceDistance?: number;
	UseAmmo?: DEBoolean;
	UseInnerRing?: DEBoolean;
}

export interface WeaponImpactBehavior {
	AttackData?: AttackData;
	criticalChance?: number;
	criticalHitChance?: number;
	criticalHitDamageMultiplier?: number;
	PlayerDamageMultiplier?: number;
	PvpDamageMultiplier?: number;
	RadialDamage?: {
		checkForCover?: DEBoolean;
		DamagePercent: {
			DT_CORROSIVE?: number;
			DT_ELECTRICITY?: number;
			DT_EXPLOSION?: number;
			DT_FIRE?: number;
			DT_FREEZE?: number;
			DT_GAS?: number;
			DT_IMPACT?: number;
			DT_MAGNETIC?: number;
			DT_POISON?: number;
			DT_PUNCTURE?: number;
			DT_RADIANT?: number;
			DT_RADIATION?: number;
			DT_SLASH?: number;
			DT_VIRAL?: number;
		};
		baseAmount: number;
		baseProcChance?: number;
		criticalChance?: number;
		criticalMultiplier?: number;
		fallOff?: number;
		ignoreSource?: DEBoolean; // Self damage
		radius?: number;
	};
}

export interface WeaponStateBehavior {
	AutoFireWhenChargeCompleted?: DEBoolean;
	BehaviorTag?: LocTag;
	BurstDelay?: number;
	ChargeModifier?: keyof typeof UpgradeType;
	ChargeTime?: number;
	ClipSizeAffectsChargeTime?: DEBoolean;
	DamageMultiplier?: number;
	fireRate?: number;
	IsAlternateFire?: DEBoolean;
	LocTag?: LocTag;
	MinChargeRatio?: number;
	MinDamageMultiplier?: number;
	NumShots?: number;
	reloadTime?: number;
}

export type Behavior = WeaponFireBehavior | WeaponImpactBehavior | WeaponStateBehavior;
export type BehaviorTypes = {
	["fire:Type"]?: string;
	["impact:Type"]?: string;
	["state:Type"]?: string;
};

export type ArtifactSlot = keyof typeof ArtifactPolarity;
export interface AbilityType {
	IconTexture: string;
	IsHelminth?: boolean;
	LocalizeTag: LocTag;
	LocalizeDescTag: LocTag;
	path: PackagePath;
}

export type KuvaData = [DamageType, number];

export interface EquipmentData extends InterfaceDescriptionData {
	AbilityTypes?: AbilityType[];
	AdditionalItems?: PackagePath[];
	AmmoCapacity?: number;
	AmmoClipSize?: number;
	ArmourRatingOverride?: number;
	ArtifactSlots?: ArtifactSlot[];
	BatteryRegenDelay?: number;
	BatteryRegenRate?: number;
	Behaviors?: BehaviorTypes[];
	CategoryTag?: LocTag;
	ClipIsBattery?: DEBoolean;
	DefaultSlottedUpgrades?: Array<{ItemType: ItemPath; Slot: number}>;
	EquipTime?: number;
	EraTag?: LocTag;
	Finishers?: ItemPath[]; // Path
	FireModes?: Array<{
		behaviorIndex: number;
		localizedTag: LocTag;
	}>;
	HasClip?: DEBoolean;
	HeavyBehaviors?: BehaviorTypes[]; // Archgun damage in regular missions
	InventorySlot?: InventorySlot;
	IsAbilityWeapon?: DEBoolean;
	IsKuva?: DEBoolean;
	IsPrime?: DEBoolean;
	IsVaulted?: DEBoolean;
	LevelCap?: number;
	LevelUpgrades: UpgradeData[];
	MaxEnergy?: number;
	MaxEnergyDuration?: number;
	MaxEnergyEfficiency?: number;
	MaxEnergyRange?: number;
	MaxEnergyStrength?: number;
	MaxHealthOverride?: number;
	MaxShieldOverride?: number;
	MaxStaminaOverride?: number;
	MeleeStyle?: string;
	ModularUpgrades?: UpgradeData[]; // Special upgrades added by parts
	MovementSpeedMultiplier?: number;
	NeedsHealthForReload?: DEBoolean; // Hema uses health to reload
	OmegaAttenuation?: number;
	PartType?: ModularPartType;
	PassiveAbilityLocTag?: LocTag;
	ParryDamagePercentBlocked?: number;
	PremiumPrice?: number;
	PrimaryOmegaAttenuation?: number;
	PrimeSellingPrice?: number;
	PurchaseQuantity?: number;
	PVPAmmoClipSize?: number;
	Quality?: RelicQuality;
	RegularPrice?: number;
	resultItemType?: PackagePath;
	RequiredLevel?: number;
	SellingPrice?: number;
	UseClipSizeAsMaxAmmo?: DEBoolean; // Override max ammo with clip (Hema)
	WeaponTypes?: EquipmentData[]; // used exclusively by the Dark Split-Sword
	ZoomLevels?: {
		Upgrades?: UpgradeData[];
	};
}

export interface ModSetData extends InterfaceDescriptionData {
	Ability?: {
		ActivationXP: number;
		Blocking: number;
		EnergyRequiredToActivate: number;
		EvaluateScript: DEBoolean;
		Script: ScriptData;
	};
	ArtifactPolarity?: keyof typeof ArtifactPolarity;
	BaseDrain?: keyof typeof Quota;
	BuffSet?: DEBoolean;
	FusionLimit?: keyof typeof Quota;
	HudBuffIcon?: PackagePath;
	InstallEffect: PackagePath;
	InstallSound: PackagePath;
	ModSetIcon: PackagePath;
	NumUpgradesInSet: number;
	PVPValue: PVPValueData;
	Rarity: keyof typeof Rarity;
	Upgrades: UpgradeData[];
}

type RivenRestriction = string;

export interface RivenUpgradeEntry {
	__id: number; // custom
	BuffRestrictions: RivenRestriction[];
	CanBeBuff: DEBoolean;
	CanBeCurse: DEBoolean;
	CompatibilityTags: CompatibilityTag[];
	CurseRestrictions: RivenRestriction[];
	Icon: PackagePath;
	LocalizePrefixTag: LocTag | "";
	LocalizeSuffixTag: LocTag | "";
	Rarity: keyof typeof Rarity;
	Tag: string;
	Upgrades: Array<{
		Schema: BaseUpgradeData;
		Upgrade: UpgradeData;
	}>;
}

export interface RivenItemCompatibilities {
	Attenuation: number;
	ItemType: PackagePath;
	Rarity: keyof typeof Rarity | "NONE";
}

export interface RivenData extends InterfaceDescriptionData {
	CanBeCurse: DEBoolean;
	CurseAtten: number;
	FusionLimitRange: [number, number];
	Giftable: DEBoolean;
	ItemCompatibilities: RivenItemCompatibilities[];
	ItemCompatibility: PackagePath;
	NumBuffs: {
		COMMON: [number, number];
	};
	NumBuffsAtten: [number, number, number, number, number, number];
	NumBuffsCurseAtten: [number, number, number, number, number, number];
	NumCurses: {
		COMMON: [number, number];
	};
	Polarities: Array<keyof typeof ArtifactPolarity>;
	SearchTags: LocTag[];
	SentinelItemCompatibilities?: RivenItemCompatibilities[];
	SpecificFitAttenuation: number;
	UpgradeEntries: RivenUpgradeEntry[];
}

export interface ModularPartData extends InterfaceDescriptionData {
	DonationStandingBonus?: number;
	PartType?: ModularPartType;
}

export interface KitgunBarrelData extends ModularPartData, EquipmentData {
	BarrelType: KitgunBarrelType;
	BehaviorOverrides?: BehaviorTypes;
	GunType?: KitgunGunType;
	PartType: ModularPartType;
	PrimaryBehaviorOverrides?: BehaviorTypes;
	UngildedModifiers?: UpgradeData[];
}

export interface KitgunHandleData extends ModularPartData {
	Overrides: Array<{
		BarrelType: KitgunBarrelType;
		Damage: number;
		FireRate: number;
		GunType: KitgunGunType;
		TraceDistance: number;
	}>;
	Upgrades?: UpgradeData[];
}

export interface KitgunClipData extends ModularPartData {
	Overrides: Array<{
		BarrelType: KitgunBarrelType;
		CritChance: number;
		CritMultiplier: number;
		GunType: KitgunGunType;
		MagazineSize: number;
		ProcChance: number;
		ReloadTime: number;
	}>;
}

export interface MeleeAttackBehavior {
	damageAttenuation?: number;
	meleeStyle: string; // todo: enum (MS_BO_STAFF)
	meleeTree: {
		AvatarDamageFalloff?: number;
		CompatibilityTags?: CompatibilityTag[];
		ItemCompatibilityLocOverride?: LocTag;
	};
	stancePolarity: keyof typeof ArtifactPolarity;
	SweepInfo?: MeleeProxies;
}

export interface ZawStrikeData extends ModularPartData, EquipmentData {
	AttackData: AttackData;
	BaseCritChance: number;
	BaseCritMultiplier?: number;
	OneHanded?: MeleeAttackBehavior;
	PartType?: ModularPartType;
	SweepRadius?: number;
	TwoHanded?: MeleeAttackBehavior;
	Upgrades?: UpgradeData[];
	UngildedModifiers?: UpgradeData[];
}

export enum ZawGripType {
	MELEE_ONE_HAND = "MELEE_ONE_HAND",
	MELEE_TWO_HAND = "MELEE_TWO_HAND",
}

export interface ZawHandleData extends ModularPartData {
	BaseFireRate?: number; // Undefined = 55
	GripType?: ZawGripType; // Undefined = MELEE_ONE_HAND
	Upgrades?: UpgradeData[];
}

export interface ZawLinkData extends ModularPartData {
	Upgrades?: UpgradeData[];
}

export interface MoaHeadData extends ModularPartData, EquipmentData {}

export interface UniqueDamageSource {
	attackData: {[damageType: string]: number};
	name?: LocTag; // label to display for damage block
	stats?: {[upgradeType: string]: number};
	isProjectile?: boolean;
	isChargedProjectile?: boolean;
	EmbedTime?: number;
	DamageRadius?: number;
	EmbedDamageRadius?: number;
}
