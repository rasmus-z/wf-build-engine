import {RivenSlotData} from "./Build";
import {
	Item,
	PackagePath,
	RivenData,
	RivenUpgradeEntry,
	UpgradeData,
} from "./interfaces";

export default class Riven {
	public static isRiven(path: PackagePath) {
		return path.startsWith("/Lotus/Upgrades/Mods/Randomized/");
	}

	public static getUpgradeById(
		riven: Item<RivenData>,
		id: number,
	): RivenUpgradeEntry | null {
		return riven.data.UpgradeEntries.filter((val) => val.__id === id)[0];
	}

	public static getBaseBuffValue = (
		disposition: number,
		numBuffs: number,
		hasCurse: boolean,
		isCurse: boolean,
	) => {
		let ret = disposition * 15;
		if (isCurse) {
			if (numBuffs === 2) {
				ret *= 0.33;
			} else {
				ret *= 0.5;
			}
		} else {
			if (hasCurse) {
				ret *= 1.25;
			}
			if (numBuffs === 2) {
				ret *= 0.66;
			} else {
				ret *= 0.5;
			}
		}

		return ret;
	};

	public static getUpgradeData = (
		riven: Item<RivenData>,
		buffId: number,
		buffNormalValue: number,
		disposition: number,
		numBuffs: number,
		hasCurse: boolean,
		isCurse: boolean,
	) => {
		const baseVal = Riven.getBaseBuffValue(disposition, numBuffs, hasCurse, isCurse);
		const upgradeEntry = Riven.getUpgradeById(riven, buffId);
		if (!upgradeEntry) {
			return null;
		}

		let buffMultiplier = upgradeEntry.Upgrades[0].Upgrade.Value;
		if (isCurse) {
			buffMultiplier *= -1;
		}
		const buffRange = baseVal * buffMultiplier * 0.2;
		const minBuffVal = baseVal * buffMultiplier * 0.9;
		const displayedBuffValue = minBuffVal + buffNormalValue * buffRange;

		// return the upgrade entry with an UNRANKED value
		const upgrade: UpgradeData = {
			...upgradeEntry.Upgrades[0].Upgrade,
			Value: displayedBuffValue,
		};
		return upgrade;
	};

	public static *getPassiveUpgrades(
		riven: Item<RivenData>,
		rivenData: RivenSlotData,
		disposition: number,
	) {
		if (!rivenData.buffs) {
			return;
		}

		if (rivenData.buffs) {
			const numBuffs = rivenData.buffs.length;
			const hasCurse = !!rivenData.curse;
			for (const [buffId, buffNormalValue] of rivenData.buffs) {
				const upgrade = Riven.getUpgradeData(
					riven,
					buffId,
					buffNormalValue,
					disposition,
					numBuffs,
					hasCurse,
					false,
				);
				if (upgrade) {
					yield upgrade;
				}
			}

			if (hasCurse) {
				const [curseId, curseNormalValue] = rivenData.curse as [number, number];
				const curse = Riven.getUpgradeData(
					riven,
					curseId,
					curseNormalValue,
					disposition,
					numBuffs,
					hasCurse,
					true,
				);
				if (curse) {
					yield curse;
				}
			}
		}
	}
}
