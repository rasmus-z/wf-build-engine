import {ScriptData, UpgradeData} from "./interfaces";

const asPercent = (i: number, roundTo: number) =>
	roundTo === 0.1
		? (Math.round(i * 1000) / 10).toString()
		: Math.round(i * 100).toString();

const Scripts: {
	[key: string]: (
		rank: number,
		script: ScriptData,
		roundTo: number,
		upgrades: UpgradeData[],
	) => {[key: string]: string};
} = {
	"/Lotus/Powersuits/PowersuitAbilities/Avalanche.lua:GetAugmentDescriptionInfo": (
		rank,
	) => ({
		ABSORB: [40, 45, 50, 60][rank].toString(),
	}),
	"/Lotus/Powersuits/PowersuitAbilities/BerserkerScream.lua:GetAugmentDescriptionInfo": (
		rank,
	) => ({
		DURATION_INC: (rank * 0.25 + 1.25).toString(),
	}),
	"/Lotus/Powersuits/PowersuitAbilities/CloneTheDead.lua:GetAugmentDescriptionInfo": (
		rank,
	) => ({
		LINK_PCT: (rank + 3).toString(),
		RANGE: "50",
	}),
	"/Lotus/Powersuits/PowersuitAbilities/EnergyVampireAbility.lua:GetAugmentDescriptionInfo": (
		rank,
	) => ({
		SHIELD_PCT: [85, 100, 120, 150][rank].toString(),
	}),
	"/Lotus/Powersuits/PowersuitAbilities/InfestRupture.lua:GetAugmentDescriptionInfo": (
		rank,
	) => ({
		COUNT: "4",
		CRIT: [70, 85, 100, 120][rank].toString(),
		DURATION: [9, 11, 13, 15][rank].toString(),
	}),
	"/Lotus/Powersuits/PowersuitAbilities/InfestTendrils.lua:GetAugmentDescriptionInfo": (
		rank,
	) => ({
		DAMAGE: (rank * 50 + 150).toString(),
		RANGE: [1, 2, 3, 5][rank].toString(),
	}),
	"/Lotus/Powersuits/PowersuitAbilities/IronSkin.lua:GetAugmentDescriptionInfo": (
		rank,
	) => ({
		DAMAGE_PERCENT: [40, 60, 80, 100][rank].toString(),
	}),
	"/Lotus/Powersuits/PowersuitAbilities/KhoraCage.lua:GetAugmentDescriptionInfo": (
		rank,
	) => ({
		DROP_CHANCE: [30, 35, 50, 65][rank].toString(),
	}),
	"/Lotus/Powersuits/PowersuitAbilities/NovaDrop.lua:GetAugmentDescriptionInfo": (
		rank,
	) => ({
		RANGE: [1, 1.5, 2, 3][rank].toString(),
	}),
	"/Lotus/Powersuits/PowersuitAbilities/RangerSteal.lua:GetAugmentDescriptionInfo": (
		rank,
	) => ({
		SPEED: (rank * 5 + 10).toString(),
	}),
	"/Lotus/Powersuits/PowersuitAbilities/RevenantAffliction.lua:GetAugmentDescriptionInfo": (
		rank,
	) => ({
		DURATION: [3, 3.5, 4, 5][rank].toString(),
	}),
	"/Lotus/Powersuits/PowersuitAbilities/RhinoChargeAbility.lua:GetAugmentDescriptionInfo": (
		rank,
	) => ({
		ARMOR_INCREASE: Math.floor(12.5 * (rank + 1)).toString(),
		DURATION: (2 * rank + 4).toString(),
	}),
	"/Lotus/Powersuits/PowersuitAbilities/RicochetArmor.lua:GetAugmentDescriptionInfo": (
		rank,
	) => ({
		PROC_PCT: [30, 35, 40, 50][rank].toString(),
	}),
	"/Lotus/Powersuits/PowersuitAbilities/SearchTheDead.lua:GetAugmentDescriptionInfo": (
		rank,
	) => ({
		HEALTH_AMOUNT: (16 - rank * 2).toString(),
	}),
	"/Lotus/Powersuits/PowersuitAbilities/Shed.lua:GetAugmentDescriptionInfo": (
		rank,
	) => ({
		DURATION: "10",
		HEAL_AMMOUNT: (20 + rank * 10).toString(), // sic
	}),
	"/Lotus/Powersuits/PowersuitAbilities/Sonar.lua:GetAugmentDescriptionInfo": (
		rank,
	) => ({
		DURATION_PERCENT: [50, 65, 80, 100][rank].toString(),
	}),
	"/Lotus/Powersuits/PowersuitAbilities/SonicEarthQuake.lua:GetAugmentDescriptionInfo": (
		rank,
	) => ({
		DAMAGE_MULT: [10, 12, 15, 20][rank].toString(),
	}),
	"/Lotus/Powersuits/PowersuitAbilities/Speed.lua:GetAugmentDescriptionInfo": (
		rank,
	) => ({
		DAMAGE: [75, 100, 125, 175][rank].toString(),
	}),
	"/Lotus/Powersuits/PowersuitAbilities/YinYangAura.lua:GetAugmentDescriptionInfo": (
		rank,
	) => ({
		SLOW: [20, 30, 35, 40][rank].toString(),
		STRENGTH: (6 + rank * 3).toString(),
	}),
	"/Lotus/Scripts/Mods/AmmoMutationMod.lua:GetDescriptionInfo": (
		rank,
		script,
		roundTo,
		upgrades,
	) => ({
		PISTOL_RIFLE: Math.round(
			[0.625, 0.125, 0.1875, 0.25, 0.3125, 0.375, 0.4375, 0.5, 0.5625, 0.625, 0.6875][
				rank
			] *
				8 *
				upgrades[0].Value,
		).toString(),
		SHOTGUN_SNIPER: Math.round(
			[0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2, 2.25, 2.5, 2.75][rank] *
				4 *
				upgrades[0].Value,
		).toString(),
	}),
	"/Lotus/Scripts/Mods/AuraSquadPower.lua:GetDescription": (rank, script) => ({
		selfStrength: ((script["_selfReduceAmount"][rank] as number) * 100).toFixed(0),
		teamStrength: ((script["_teamStrengthAmount"][rank] as number) * 100).toFixed(0),
	}),
	"/Lotus/Scripts/Mods/FairyQuestCritToAbility.lua:GetDescription": (rank, script) => ({
		duration: ((script["_baseDuration"] as number) * (rank + 1)).toString(),
		// Actually that's not correct either:
		// val: ((25 / 6) * (rank + 1)).toFixed(1) + "%", // _baseVal seems incorrect. not using it.
		val: [4.2, 8.3, 12.5, 16.6, 20.1, 25][rank].toString() + "%",
	}),
	"/Lotus/Scripts/Mods/ModDescriptionStats.lua:GetDescriptionInfo": (
		rank,
		script,
		roundTo,
		upgrades,
	) => {
		const ret: {[key: string]: string} = {};
		const addPlusses = script._addPlusses || [];
		upgrades.forEach((stat, index) => {
			const symbol = addPlusses[index] ? "+" : "";
			const displayAsPercent =
				!!script["_displayAsPercent"][index] || upgrades[index].DisplayAsPercent;
			const value = stat.Value * (rank + 1);
			ret[`STAT${index + 1}`] = displayAsPercent
				? symbol + asPercent(value, roundTo)
				: symbol + value.toString();
		});
		return ret;
	},
	"/Lotus/Scripts/Mods/OnPickup.lua:GetArmourForOneHitDesc": (rank) => ({
		ARMOUR: ((rank + 1) * 75).toString(),
		DURATION: "3",
		STACKS: "3",
	}),
	"/Lotus/Scripts/Mods/OnPickup.lua:GetStrengthForOneCastDesc": (rank) => ({
		STRENGTH: [9, 18, 26, 34, 42, 50][rank].toString(),
	}),
	"/Lotus/Scripts/Mods/ResistanceOnDamage.lua:GetDescription": (
		rank,
		script,
		roundTo,
	) => ({
		duration: (script["_duration"] as number[])[rank].toString(),
		maxResistance: asPercent(script["_maxResistance"] as number, roundTo),
		resistance: asPercent((script["_resistancePerLevel"] as number[])[rank], roundTo),
	}),
	"/Lotus/Types/Friendly/Pets/KubrowPetBehaviors/KubrowChargerStrainAbility.lua:GetConsumeLoc": (
		rank,
		script,
		roundTo,
	) => ({
		HEALTH: asPercent(script["_consumeHealth"][rank], roundTo),
		RANGE: script["_maggotPullRange"].toString(),
	}),
	"SeekerProjOnHeadshotKill.lua:GetDescriptionInfo": (rank) => ({
		NUM: (1 * (rank + 1)).toString(),
	}),
	"/Lotus/Scripts/Mods/ResistSelfDamage.lua:GetDescription": (
		rank,
		script,
		roundTo,
	) => ({
		selfDamage: asPercent(1 - script["_selfDamageMultiplier"][rank], roundTo),
		damage: asPercent(1 - script["_damageOutputMultiplier"][rank], roundTo),
	}),
	"/Lotus/Scripts/Mods/GrakataAltFire.lua:GetLocValues": (rank, script, roundTo) => ({
		RATE: (
			Math.round((0.050000000745058 / script["_burstDelays"][rank]) * 10) * 10
		).toFixed(0),
		RELOAD: asPercent(script["_reloadPercent"][rank], roundTo),
	}),
	"/Lotus/Types/Sentinels/SentinelAbilities/SynthSetMod.lua:GetDescription": (
		rank,
		script,
		roundTo,
	) => ({
		val: asPercent(script["_holsterReloadRate"][rank], roundTo),
	}),
	"/Lotus/Scripts/Mods/HawkSetPassive.lua:GetDescriptionInfo": (
		rank,
		script,
		roundTo,
	) => ({
		val: script["_sleepTime"][rank].toString(),
	}),
	"/Lotus/Types/Friendly/Pets/KubrowPetBehaviors/KubrowChargerStrainAbility.lua:GetDescription": (
		rank,
		script,
		roundTo,
	) => ({
		COUNT: script["_maxCysts"][rank].toString(),
		COOLDOWN: Math.round(
			script["_cystInterval"] * script["_maxCysts"][rank],
		).toString(),
		DELAY: script["_evolveInterval"].toString(),
	}),
	"/Lotus/Types/Friendly/Pets/CatbrowPetBehaviors/CatbrowTekSetAbility.lua:GetDescription": (
		rank,
		script,
		roundTo,
	) => ({
		RADIUS: script["_radiusPerLevel"][rank].toString(),
		COOLDOWN: script["_cooldownPerLevel"][rank].toString(),
		DAMAGE: script["_dpsPerLevel"][rank].toString(),
	}),
	"/Lotus/Scripts/Mods/SpidersLeap.lua:GetDescriptionInfo": (
		rank,
		script,
		roundTo,
	) => ({
		VAL: asPercent(script["_multipliers"][rank], roundTo),
		DURATION: [5, 10, 15, 20][rank].toString(),
	}),
	// Saxum Set: Lifted enemies explode on death dealing 30% Enemy Max Health as Impact Damage in a 6m radius.
	"/Lotus/Upgrades/Mods/Sets/Femur/FemurModSetBonusAbility.lua:GetDescriptionInfo": (
		rank,
		script,
		roundTo,
	) => ({
		STAT1: asPercent(script["_healthPct"][rank], roundTo),
		STAT2: Math.round(script["_damageRadius"][rank]).toString(),
	}),
	// Jugulus Set: Heavy Slam attacks manifest tendrils that skewer enemies within 10m, dealing 75 Puncture Damage and stunning them for 3s. Cooldown: 6s.
	"BonebladeModTentacleAbility.lua:GetDescriptionInfo": (rank, script, roundTo) => ({
		STAT1: script["_spawnRadius"][rank].toString(),
		STAT2: script["_punctureDamage"][rank].toString(),
		STAT3: script["_stunTime"][rank].toString(),
		STAT4: script["_coolDown"][rank].toString(),
	}),
	// Carnis Set: Killing an enemy with a Heavy Attack grants 30% Evasion and immunity to Status Effects for 6s.
	"AshenModSetBonusAbility.lua:GetDescriptionInfo": (rank, script, roundTo) => ({
		STAT1: asPercent(script["_evasionPct"][rank], roundTo),
		STAT2: script["_statusImmunity"][rank].toString(),
	}),
	// Investigator, Botanist
	"/Lotus/Types/Sentinels/SentinelAbilities/CodexScannerAbility.lua:GetDescriptionInfo": (
		rank,
	) => ({
		RANGE: [20, 26, 32, 38, 44, 50][rank].toString(),
		DURATION: [5, 4.4, 3.8, 3.2, 2.6, 2][rank].toFixed(1),
	}),
	// Scan Aquatic Lifeforms
	"/Lotus/Types/Sentinels/SentinelAbilities/LocateCreaturesAbility.lua:GetDescriptionInfo": (
		rank,
	) => ({
		STAT1: (25 * (rank + 1)).toString(),
		STAT2: (10 * (rank + 1)).toString(),
	}),
	// Hata-Satya
	"/Lotus/Scripts/Mods/SomaCritChanceOnHit.lua:GetModDescriptionInfo": (rank) => ({
		CRIT: [0.2, 0.4, 0.6, 0.8, 1, 1.2][rank].toString(),
	}),
	// Bhisaj-Bal
	"/Lotus/Scripts/Mods/ParisHealOnStatus.lua:GetModDescriptionInfo": (rank) => ({
		HEAL: (50 * (rank + 1)).toString(),
		PROCS: "3",
	}),
	// Mecha set bonus
	"/Lotus/Types/Friendly/Pets/KubrowPetBehaviors/KubrowMarkMechaAbility.lua:GetDescription": (
		rank,
		script,
	) => ({
		COOLDOWN: script["_markCooldown"][rank].toFixed(0),
		DURATION: script["_markDuration"][rank].toFixed(0),
		RANGE: script["_statusSpreadRange"][rank].toFixed(1),
	}),
};

export default Scripts;
